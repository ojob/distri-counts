Tutorial
========

This page provides basic usage of the REST API.

Setting the stuff
-----------------

Launch the server::

  districounts -d --create-sample serve -h localhost -p 8080

The ``-d`` flag activates the debug mode, thus showing the errors that may
occur. ``--create-sample`` flag creates sample data, starting by a default
project.
The ``-h`` and ``-p`` options enable setting of address and port; defaults
are ``localhost`` and ``8080``.

The web view can then be accessed at http://localhost:8080/.


Presentation of API entry points
--------------------------------

Here are some examples of calls to the REST API, using python and the test
package :py:mod:`webtest` to allow these examples to run during automated
testing with no server running.

(See below for same usage with :py:mod:`requests` package.)

Let's start by creating a new app instance, that will be used for presenting
the API entry points:

.. code-block:: python

  >>> import json, pprint
  >>> import pytest, webtest
  >>> from districounts import backend
  >>> my_app = backend.Api(debug=True)
  >>> app = webtest.TestApp(my_app)


Obviously, accessing to an undefined route returns a ``404 Not Found`` answer:

.. code-block:: python

  >>> resp = app.get('/some/non-existing/route',
  ...                expect_errors=True)
  >>> resp.status
  '404 Not Found'


``GET /api/v1/healthy``
.......................

The basic entry point is the `healthy` one:

.. code-block:: python

  >>> resp = app.get('/api/v1/healthy')
  >>> resp.status
  '200 OK'
  >>> resp.json
  {'result': {'healthy': True}, 'app_version': ..., 'mode': 'development'}


The ``app_version`` and ``mode`` are present in all API answers.


``GET /api/v1/links``
.....................

Calling this entry point provides links to useful ressources:

.. code-block:: python

  >>> resp = app.get('/api/v1/links')
  >>> resp.status
  '200 OK'
  >>> pprint.pprint(resp.json['result'])
  {'bugtracker': 'https://framagit.org/ojob/districounts/-/issues',
   'documentation': 'https://ojob.frama.io/districounts/',
   'homepage': 'https://framagit.org/ojob/districounts',
   'roadmap': 'https://framagit.org/ojob/districounts/-/milestones'}


``GET /api/v1/projects``
........................

At creation of the application, there is no project in the instance:

.. code-block:: python

  >>> resp = app.get('/api/v1/projects')
  >>> resp.status
  '200 OK'
  >>> isinstance(resp.json['result']['projects'], list)
  True
  >>> len(resp.json['result']['projects'])
  0


``POST /api/v1/projects/<slug>``
.................................

To create a new project, provide the route with its name:

.. code-block:: python

  >>> resp = app.post_json(
  ...     '/api/v1/projects',
  ...     params=dict(name='My new Project'))
  >>> resp.status
  '200 OK'
  >>> resp.json['message']
  'project created'
  >>> resp.json['result']['projects']
  [{'slug': 'my-new-project', 'name': 'My new Project'}]

And it is then possible to retrieve all details of the project:

  >>> resp = app.get('/api/v1/projects')
  >>> resp.status
  '200 OK'
  >>> resp.json['result']['projects']
  [{'slug': 'my-new-project', 'name': 'My new Project'}]


The slug of the project shall be unique, as it is used as project identifier:

.. code-block:: python

  >>> resp = app.post_json(
  ...     '/api/v1/projects',
  ...     params=dict(name='my new    project'),
  ...     expect_errors=True)
  >>> resp.status
  '400 Bad Request'
  >>> resp.json['message']
  'cannot create project: slug already used: my-new-project'


``PATCH /api/v1/projects/<slug>``
.................................

It is possible to update the name of a project:

.. code-block:: python

  >>> resp = app.patch_json(
  ...     '/api/v1/projects/my-new-project',
  ...     params=dict(name='New project'))
  >>> resp.status
  '200 OK'
  >>> resp.json['message']
  'project updated'
  >>> resp.json['result']['projects'][0]
  {'slug': 'new-project', 'name': 'New project'}


Obviously again, the slug shall be available:

.. code-block:: python

  # create another project
  >>> resp = app.post_json(
  ...     '/api/v1/projects',
  ...     params=dict(name='other project'))

  # and try to change project name
  >>> resp = app.patch_json(
  ...     '/api/v1/projects/new-project',
  ...     params=dict(name='Other - Project'),
  ...     expect_errors=True)
  >>> resp.status
  '400 Bad Request'
  >>> resp.json['message']
  'cannot update: slug already used: other-project'

And the project shall obviously also already exist:

.. code-block:: python

  >>> resp = app.patch_json(
  ...     '/api/v1/projects/some-unknown-project',
  ...     params=dict(name='Other - Project'),
  ...     expect_errors=True)
  >>> resp.status
  '404 Not Found'
  >>> resp.json['message']
  "project not found: unknown slug: 'some-unknown-project'"


``GET /api/v1/projects/<slug>``
................................

Using the slug, it is possible to access the project:

.. code-block:: python

  >>> resp = app.get('/api/v1/projects/new-project')
  >>> resp.status
  '200 OK'
  >>> resp.json['result']['projects']
  [{'slug': 'new-project', 'name': 'New project'}]


The old slug can also be used, even if returned result is obviously the new
data:

.. code-block:: python

  >>> resp = app.get('/api/v1/projects/my-new-project')
  >>> resp.status
  '200 OK'
  >>> resp.json['result']['projects']
  [{'slug': 'new-project', 'name': 'New project'}]


.. code-block:: python

  >>> resp = app.get('/api/v1/projects/unknown-project',
  ...                expect_errors=True)
  >>> resp.status
  '404 Not Found'
  >>> resp.json['message']
  "project not found: unknown slug: 'unknown-project'"


``DELETE /api/v1/projects/<slug>``
..................................

To delete a project, use the ``DELETE`` verb.

.. code-block:: python

  >>> # first create a new project
  >>> resp = app.post_json(
  ...     '/api/v1/projects',
  ...     params=dict(name='Some project, soon to be deleted!'))
  >>> resp.status
  '200 OK'
  >>> resp.json['result']['projects'][0]
  {'slug': 'some-project-soon-to-be-deleted', 'name': 'Some project, soon to be deleted!'}

  >>> # and delete it
  >>> resp = app.delete(
  ...     '/api/v1/projects/some-project-soon-to-be-deleted')
  >>> resp.status
  '200 OK'
  >>> resp.json['message']
  'project deleted'


``GET /api/v1/projects/<slug>/users``
......................................

At creation of the application, there is no user to return:

.. code-block:: python

  >>> resp = app.get('/api/v1/projects/my-new-project/users')
  >>> resp.status
  '200 OK'
  >>> isinstance(resp.json['result']['users'], list)
  True
  >>> len(resp.json['result']['users'])
  0


``POST /api/v1/projects/<slug>/users``
.......................................

Then, pushing some data is done with simple JSON payload. For creation of a
new user, ``POST`` at least the name to ``api/v1/projects/<slug>/users``:

.. code-block:: python

  >>> resp = app.post_json('/api/v1/projects/my-new-project/users', dict(name='Bob'))
  >>> resp.status
  '200 OK'
  >>> resp.json['message']
  'User created'


The answer provides the created user, together with its associated ``uid``:

.. code-block:: python

  >>> 'users' in resp.json['result']
  True
  >>> isinstance(resp.json['result']['users'], list)
  True
  >>> len(resp.json['result']['users'])
  1
  >>> list(resp.json['result']['users'][0].keys())
  ['uid', 'name', 'email']
  >>> uid_bob = resp.json['result']['users'][0]['uid']
  >>> type(uid_bob)
  <class 'int'>


Failure to comply with creation rules leads to ``400 Bad Request`` answer,
with a ``message`` entry in the answer:

* Creation of a user requires the 'name' parameter:

  .. code-block:: python

    >>> resp = app.post_json('/api/v1/projects/my-new-project/users', params=dict(na__me='bob'),
    ...                      expect_errors=True)
    >>> resp.status
    '400 Bad Request'
    >>> resp.json['message']
    'missing parameter(s): name'


* Request body shall be formatted as JSON:

  .. code-block:: python

    >>> resp = app.post('/api/v1/projects/my-new-project/users', params=dict(name='léa'),
    ...                 expect_errors=True)
    >>> resp.status
    '400 Bad Request'
    >>> resp.json['message']
    'empty request.json'


``GET /api/v1/projects/<slug>/records``
.......................................

The user ``uid`` can be used to retrieve the corresponding user data:

.. code-block:: python

  >>> resp = app.get(f'/api/v1/projects/my-new-project/users/uid={uid_bob}')
  >>> resp.status
  '200 OK'
  >>> resp.json['result']['users'][0]['name']
  'Bob'
  >>> resp.json['result']['users'][0]['email'] is None  # email not set
  True


Now that the user store contains some users, using the ``users`` entry point
returns them all:

.. code-block:: python

  >>> resp = app.get('/api/v1/projects/my-new-project/users')
  >>> resp.status
  '200 OK'
  >>> len(resp.json['result']['users'])
  1
  >>> 'Bob' in [user_dict['name'] for user_dict in resp.json['result']['users']]
  True


As expected, trying to access to non-existing resources return a ``404 Not
Found`` status:

.. code-block:: python

  >>> resp = app.get('/api/v1/projects/my-new-project/users/uid=1032', expect_errors=True)
  >>> resp.status
  '404 Not Found'
  >>> resp.json['message']
  'user id not found: 1032'


``GET /api/v1/projects/<slug>/users/name=<name>``
.................................................

It is possible to retrieve users by name:

.. code-block:: python

  >>> resp = app.get('/api/v1/projects/my-new-project/users/name=Bob')
  >>> resp.status
  '200 OK'
  >>> len(resp.json['result']['users'])
  1
  >>> resp.json['result']['users'][0]['uid'] == uid_bob
  True


Again, trying to retrieve non-existing resources return a ``404 Not Found``
status:

.. code-block:: python

  >>> resp = app.get('/api/v1/projects/my-new-project/users/name=roger',
  ...                expect_errors=True)
  >>> resp.status
  '404 Not Found'
  >>> resp.json['message']
  "user name not found: 'roger'"


``PATCH /api/v1/projects/<slug>/users/uid=<uid>``
.................................................

It is possible to update user characteristics:

.. code-block:: python

  >>> resp = app.get('/api/v1/projects/my-new-project/users')
  >>> some_user = resp.json['result']['users'][0]
  >>> old_name = some_user['name']
  >>> new_name = 'some new name'
  >>> resp = app.patch_json(
  ...     f'/api/v1/projects/my-new-project/users/uid={some_user["uid"]}',
  ...     params={'name': new_name})
  >>> resp.status
  '200 OK'


The answer provides the updated user:

.. code-block:: python

  >>> len(resp.json['result']['users'])
  1
  >>> assert resp.json['result']['users'][0]['uid'] == some_user['uid']
  >>> assert resp.json['result']['users'][0]['name'] == new_name
  >>> assert resp.json['result']['users'][0]['email'] == some_user['email']


And we can verify that search by name is also correctly updated:

.. code-block:: python

  >>> resp = app.get(f'/api/v1/projects/my-new-project/users/name={new_name}')
  >>> assert resp.status == '200 OK'
  >>> assert len(resp.json['result']['users']) == 1

  >>> resp = app.get(f'/api/v1/projects/my-new-project/users/name={old_name}',
  ...                expect_errors=True)
  >>> assert resp.status == '404 Not Found'
  >>> assert 'result' not in resp.json


``DELETE/api/v1/projects/<slug>/users/uid=<uid>``
.................................................

Using its id, a user can be deleted:

.. code-block:: python

  >>> resp = app.get('/api/v1/projects/my-new-project/users')
  >>> last_user = resp.json['result']['users'][-1]
  >>> resp = app.delete(
  ...     f'/api/v1/projects/my-new-project/users/uid={last_user["uid"]}')
  >>> resp.status
  '200 OK'


The answer provides the deleted user:

.. code-block:: python

  >>> len(resp.json['result']['users'])
  1
  >>> resp.json['result']['users'][0]['uid'] == last_user['uid']
  True
  >>> resp.json['result']['users'][0]['name'] == last_user['name']
  True
  >>> resp.json['result']['users'][0]['email'] == last_user['email']
  True


The user store is now empty, as we deleted the sole user in our app instance:

.. code-block:: python

  >>> len(app.get('/api/v1/projects/my-new-project/users').json['result']['users'])
  0


``GET /api/v1/projects/<slug>/records``
.......................................

This entry point returns a list of the records stored. At creation, it is
empty though,

.. code-block:: python

  >>> resp = app.get('/api/v1/projects/my-new-project/records')
  >>> resp.status
  '200 OK'
  >>> isinstance(resp.json['result']['records'], list)
  True
  >>> len(resp.json['result']['records'])
  0


``POST /api/v1/projects/<slug>/records``
........................................

Pushing some data is done with simple JSON payload. For creation of a
new record, first create relevant users in the instance:

.. code-block:: python

  >>> resp = app.post_json('/api/v1/projects/my-new-project/users',
  ...                      dict(name='Roberta'))
  >>> assert resp.status == '200 OK'
  >>> uid_roberta = resp.json['result']['users'][0]['uid']
  >>> resp = app.post_json('/api/v1/projects/my-new-project/users',
  ...                      dict(name='Anthony'))
  >>> uid_anthony = resp.json['result']['users'][0]['uid']
  >>> assert resp.status == '200 OK'


Then ``POST`` the transaction record parameters to
``/api/v1/projects/<slug>/records``:

.. code-block:: python

  >>> resp = app.post_json('/api/v1/projects/my-new-project/records',
  ...                      dict(sender=uid_roberta,
  ...                      receivers=[uid_roberta, uid_anthony],
  ...                      amount=10.3))
  >>> resp.status
  '200 OK'
  >>> resp.json['message']
  'record created'
  >>> rid_0 = resp.json['result']['records'][0]['rid']


The answer provides the ``RecordId`` of the created record, as a string:

.. code-block:: python

  >>> 'records' in resp.json['result']
  True
  >>> isinstance(resp.json['result']['records'], list)
  True
  >>> len(resp.json['result']['records'])
  1
  >>> list(resp.json['result']['records'][0].keys())
  ['rid']


Using the ``/api/v1/projects/<slug>/records`` entry point, all records are
returned:

.. code-block:: python

  >>> resp = app.get('/api/v1/projects/my-new-project/records')
  >>> resp.status
  '200 OK'
  >>> len(resp.json['result']['records'])
  1
  >>> list(resp.json['result']['records'][0].keys())
  ['rid', 'sender', 'receivers', 'amount', 'currency']



Note that the users involved in a new record shall already exist, otherwise
an error is returned:

.. code-block:: python

  >>> resp = app.post_json('/api/v1/projects/my-new-project/records',
  ...                      dict(sender=1,
  ...                           receivers=[1, 2],
  ...                           amount=10.3),
  ...                      expect_errors=True)
  >>> resp.status
  '400 Bad Request'
  >>> resp.json['message']
  'cannot create record: inconsistent record: unknown sender uid: 1, unknown receivers uids: 1, 2'


``GET /api/v1/projects/<slug>/records/rid=<rid>``
.................................................

This ``rid`` returned at record creation can then be used to retrieve the data:

.. code-block:: python

  >>> type(rid_0)
  <class 'str'>
  >>> resp = app.get(f'/api/v1/projects/my-new-project/records/rid={rid_0}')
  >>> resp.status
  '200 OK'
  >>> resp.json['result']['records'][0]['sender'] == uid_roberta
  True
  >>> resp.json['result']['records'][0]['receivers'] == [uid_roberta, uid_anthony]
  True
  >>> resp.json['result']['records'][0]['amount']
  10.3
  >>> resp.json['result']['records'][0]['currency']
  '€'


As expected, trying to access to non-existing resources return a ``404 Not
Found`` status:

.. code-block:: python

  >>> some_rid = '05d06197-86d6-43d1-b86d-97ded8d2f653'
  >>> resp = app.get(f'/api/v1/projects/my-new-project/records/rid={some_rid}',
  ...                expect_errors=True)
  >>> resp.status
  '404 Not Found'
  >>> resp.json['message']
  'no record with rid=05d06197-86d6-43d1-b86d-97ded8d2f653'


``PATCH /api/v1/projects/<slug>/records``
.........................................

Using ``PATCH`` method, the record attributes can be updated:

.. code-block:: python

  >>> resp = app.patch_json(
  ...     f'/api/v1/projects/my-new-project/records/rid={rid_0}',
  ...     params=dict(amount=103.0))
  >>> resp.status
  '200 OK'
  >>> resp.json['result']['records'][0]['rid'] == rid_0
  True
  >>> resp.json['result']['records'][0]['amount'] == 103.0
  True


As for creation, all users shall exist when updating the record:

.. code-block:: python

  >>> unk_uid = 10**7
  >>> assert unk_uid not in my_app.projects['new-project'].users
  >>> resp = app.patch_json(
  ...     f'/api/v1/projects/my-new-project/records/rid={rid_0}',
  ...     params=dict(sender=unk_uid),
  ...     expect_errors=True)
  >>> resp.status
  '400 Bad Request'
  >>> resp.json['message']
  'cannot update record: not consistent: unknown sender uid: 10000000'


``DELETE/api/v1/projects/<slug>/records/rid=<rid>``
...................................................

Using its id, a record can be deleted (let's create one first)

.. code-block:: python

  >>> resp = app.post_json(
  ...     '/api/v1/projects/my-new-project/records',
  ...     dict(sender=uid_roberta,
  ...          receivers=[uid_anthony],
  ...          amount=25))
  >>> new_rid = resp.json['result']['records'][0]['rid']

  >>> resp = app.delete(
  ...     f'/api/v1/projects/my-new-project/records/rid={new_rid}')
  >>> resp.status
  '200 OK'
  >>> resp.json['message']
  'record ... deleted'

The answer provides the deleted record:

  >>> assert resp.json['result']['records'][0] == \
  ...     {'rid': new_rid, 'sender': uid_roberta, 'receivers': [uid_anthony], 'amount': 25.0, 'currency': '€'}

The record does not exist anymore:

.. code-block:: python

  >>> resp = app.get(
  ...     f'/api/v1/projects/my-new-project/records/rid={new_rid}',
  ...     expect_errors=True)
  >>> resp.status
  '404 Not Found'


``GET /api/v1/projects/<slug>/users/balance``
.............................................

For each user, their contribution is returned by 'user balance':

.. code-block:: python

  >>> resp = app.get('/api/v1/projects/my-new-project/users/balance')
  >>> resp.status
  '200 OK'
  >>> users_balance = resp.json['result']['users_balance']
  >>> [item['uid'] for item in users_balance] == [uid_roberta, uid_anthony]
  True
  >>> users_balance
  [{'uid': ..., 'user_balance': 51.5}, {'uid': ..., 'user_balance': -51.5}]


``GET /api/v1/projects/<slug>/balance``
.......................................

The project balance, i.e. what are the refundings necessary to balance the
users contributions, is returned by the 'overall balance':

.. code-block:: python

  >>> resp = app.get('/api/v1/projects/my-new-project/balance')
  >>> resp.status
  '200 OK'
  >>> balance = resp.json['result']['balance']
  >>> balance
  [[..., ..., 51.5]]
  >>> assert balance[0][0:2] == [uid_anthony, uid_roberta]


Calling the API, on ``users`` point of view
-------------------------------------------

Let's create a new app instance:

.. code-block:: python

  >>> my_app = backend.Api(init_project='My new Project')
  >>> app = webtest.TestApp(my_app)
  >>> my_app.projects['my-new-project'].records.users == {}
  True


For the moment, there's no user named 'bob' in the database:

.. code-block:: python

  >>> resp = app.get(
  ...     '/api/v1/projects/my-new-project/users/name=bob',
  ...     expect_errors=True)
  >>> resp.status
  '404 Not Found'
  >>> resp.json['message']
  "user name not found: 'bob'"


Now let's create three users, two with same name:

.. code-block:: python

  >>> resp = app.post_json(
  ...     '/api/v1/projects/my-new-project/users',
  ...     params=dict(name='bob'))
  >>> uid_0: int = resp.json['result']['users'][0]['uid']

  >>> resp = app.post_json(
  ...     '/api/v1/projects/my-new-project/users',
  ...     params=dict(name='joel'))
  >>> uid_1 = resp.json['result']['users'][0]['uid']

  >>> resp = app.post_json(
  ...     '/api/v1/projects/my-new-project/users',
  ...     params=dict(name='bob'))
  >>> resp.status  # no issue to create user with duplicate name
  '200 OK'
  >>> uid_2 = resp.json['result']['users'][0]['uid']


We can verify that all users have their own ``uid``, and that we can get the
users with the same name:

.. code-block:: python

  >>> len(set([uid_0, uid_1, uid_2]))
  3


Then we can get both users named 'bob':

.. code-block:: python

  >>> resp = app.get('/api/v1/projects/my-new-project/users/name=bob')
  >>> resp.status
  '200 OK'
  >>> [user['name'] for user in resp.json['result']['users']]
  ['bob', 'bob']



Calling the API, on ``records`` point of view
---------------------------------------------

Create a new app instance in production mode:

.. code-block:: python

  >>> my_app = backend.Api(debug=False, init_project='My new Project')
  >>> assert my_app.projects['my-new-project'].records == {}
  >>> app = webtest.TestApp(my_app)


Check wrong record deletion:

.. code-block:: python

  >>> some_non_rid = '2c1126f1-5625-45f5-a4bd-20028fca79b0'
  >>> resp = app.delete(
  ...     f'/api/v1/projects/my-new-project/records/rid={some_non_rid}',
  ...     expect_errors=True)
  >>> assert resp.status == '404 Not Found'


Check that all parameters are checked, with correct error code:

.. code-block:: python

  >>> resp = app.post_json(
  ...     '/api/v1/projects/my-new-project/records',
  ...     params=dict(sender=3, amount=3),
  ...     expect_errors=True)
  >>> assert resp.status == '400 Bad Request'
  >>> assert resp.json['message'] == "missing parameter(s): receivers"

  >>> resp = app.post_json(
  ...     '/api/v1/projects/my-new-project/records',
  ...     params=dict(sender=3, receivers=4, amount=3),
  ...     expect_errors=True)
  >>> assert resp.status == '400 Bad Request'


No details are expected in production mode:

.. code-block:: python

  >>> assert resp.json['message'] == \
  ...    "cannot parse 'receivers': cannot execute UserId on '4'"


Let's check the answer to data not formatted as JSON:

.. code-block:: python

  >>> resp = app.post(
  ...     '/api/v1/projects/my-new-project/records',
  ...     params=dict(sender=3, receivers=4, amount=3),
  ...     expect_errors=True)
  >>> assert resp.status == '400 Bad Request'
  >>> assert resp.json['message'] == "empty request.json"


Calling the API, on ``balance`` point of view
---------------------------------------------

Let's create a new app instance, with some samples inside:

.. code-block:: python

  >>> my_app = backend.Api(init_project="My new Project", create_sample=True)
  >>> app = webtest.TestApp(my_app)
  >>> resp = app.get('/api/v1/projects/my-new-project/records')
  >>> len(resp.json['result']['records']) > 0
  True
  >>> resp = app.get('/api/v1/projects/my-new-project/users')
  >>> uids = [user['uid']
  ...         for user in resp.json['result']['users']]


The users balance shall always be null, as this is the sum of transactions
flows that shall, well, 'balance':

.. code-block:: python

  >>> resp = app.get('/api/v1/projects/my-new-project/users/balance')
  >>> users_balance = resp.json['result']['users_balance']
  >>> balance = sum(entry['user_balance'] for entry in users_balance)
  >>> assert balance == pytest.approx(0.0)


Now create refundings, and verify balance at each step:

.. code-block:: python

  >>> resp = app.get('/api/v1/projects/my-new-project/balance')
  >>> project_balance = resp.json['result']['balance']
  >>> for sender, receiver, amount in project_balance:
  ...     _ = app.post_json('/api/v1/projects/my-new-project/records',
  ...                       params=dict(sender=sender, receivers=[receiver],
  ...                                   amount=amount))
  ...     users_balance = (app.get('/api/v1/projects/my-new-project/users/balance')
  ...                  .json['result']['users_balance'])
  ...     balance = sum(entry['user_balance'] for entry in users_balance)
  ...     assert balance == pytest.approx(0.0)


Create one more records, and create the refunds again:

.. code-block:: python

  >>> _ = app.post_json('/api/v1/projects/my-new-project/records',
  ...                   params=dict(sender=uids[2],
  ...                               receivers=[uids[2], uids[1]],
  ...                               amount=3.10))
  >>> resp = app.get('/api/v1/projects/my-new-project/balance')
  >>> project_balance = resp.json['result']['balance']
  >>> for sender, receiver, amount in project_balance:
  ...     _ = app.post_json('/api/v1/projects/my-new-project/records',
  ...                       params=dict(sender=sender, receivers=[receiver],
  ...                                   amount=amount))
  ...     users_balance = (app.get('/api/v1/projects/my-new-project/users/balance')
  ...                      .json['result']['users_balance'])
  ...     balance = sum(entry['user_balance'] for entry in users_balance)
  ...     assert balance == pytest.approx(0.0)


Imports and Exports
-------------------

JSON Loading at app instance creation
.....................................

The whole stuff can be imported and exported as JSON files.

Let's use one that is shipped with the ``districounts`` package:

.. code-block:: python

  >>> from pathlib import Path
  >>> TEST_IN = Path(backend.__file__).parent.parent.parent / 'tests' / 'inputs'
  >>> json_in_path = TEST_IN / 'districounts-load.json'


Now we can open this file and initiate our app instance with it:

.. code-block:: python

  >>> with json_in_path.open('r') as json_fh:
  ...     json_data = json.load(json_fh)
  >>> my_app = backend.Api(json_data=json_data, debug=True)
  >>> app = webtest.TestApp(my_app)


Expected users and records are indeed loaded:

.. code-block:: python

  >>> resp = app.get('/api/v1/projects/my-new-project/users')
  >>> users = resp.json['result']['users']
  >>> list(user['uid'] for user in users)
  [12, 5, 6, 7, 8, 1031832]

  >>> resp = app.get('/api/v1/projects/my-new-project/records')
  >>> records = resp.json['result']['records']
  >>> list(record['rid'] for record in records)
  ['aad1b2b4-b78f-4c50-8bc1-6a7574d7e5c3', '6dd0323c-0b99-44c7-84fd-fa69e333957a']


``GET /api/v1/export``
......................................

It is possible to export all projects data, as JSON:

.. code-block:: python

  >>> TEST_OUT = TEST_IN.parent / 'outputs'
  >>> json_out_path = TEST_OUT / 'districounts-dump.json'
  >>> if json_out_path.exists():
  ...     json_out_path.unlink()  # missing_ok arg added in Python 3.8
  >>> json_out_path.parent.mkdir(exist_ok=True)
  >>> resp = app.get('/api/v1/export')
  >>> with json_out_path.open('w') as json_fh:
  ...     export_data = resp.json['result']
  ...     json.dump(export_data, json_fh)
  >>> assert json_out_path.is_file()


The exported data is the exact same as the one that was imported at start-up:

.. code-block:: python

  >>> assert json_data == export_data


``POST /api/v1/import``
.......................

It is also possible to load a JSON to create the projects.

.. code-block:: python

  >>> my_new_app = backend.Api()
  >>> new_app = webtest.TestApp(my_new_app)

  # push the data
  >>> with json_out_path.open('r') as json_fh:
  ...     json_data = json.load(json_fh)
  >>> resp = new_app.post_json(
  ...     '/api/v1/import',
  ...     params=json_data)
  >>> resp.status
  '200 OK'
  >>> resp.json['message']
  'import successfull: projects: 1 new, 0 mod'


This result in the same content, including identifiers:

.. code-block:: python

  >>> for entry in ['users', 'records']:
  ...     url = f'/api/v1/projects/my-new-project/{entry}'
  ...     assert app.get(url).json['result'][entry] == \
  ...         new_app.get(url).json['result'][entry]


Importing an inconsistent JSON file will return failure:

.. code-block:: python

  >>> my_new_app = backend.Api()
  >>> new_app = webtest.TestApp(my_new_app)

  # push the data
  >>> json_in_path = TEST_IN / 'inconsistent-import.json'
  >>> with json_in_path.open('r') as json_fh:
  ...     json_data = json.load(json_fh)
  >>> resp = new_app.post_json(
  ...     '/api/v1/import',
  ...     params=json_data,
  ...     expect_errors=True)
  >>> resp.status
  '400 Bad Request'
  >>> resp.json['message']
  'import failed: load failed: unknown sender uid: 13, unknown receivers uids: 14'


``POST /api/v1/projects/<slug>/import``
.......................................

It is also possible to load a JSON to fill an existing project.

.. code-block:: python

  >>> my_new_app = backend.Api(init_project="Brand New")
  >>> new_app = webtest.TestApp(my_new_app)

  # push the data
  >>> json_in_path = TEST_IN / 'districounts-project-load.json'
  >>> with json_in_path.open('r') as json_fh:
  ...     json_data = json.load(json_fh)
  >>> resp = new_app.post_json(
  ...     '/api/v1/projects/brand-new/import',
  ...     params=json_data)
  >>> resp.status
  '200 OK'
  >>> resp.json['message']
  'import successfull: users: 6 new, 0 mod; records: 2 new, 0 mod'



Using Python :py:mod:`requests`
-------------------------------

All previous examples work also with ``requests``, with following commands
mapping:

===============================  ========================================= ================
Command                          Using WebTest                             Using `requests`
===============================  ========================================  ================
Initialization                   ``app = webtest.TestApp(backend.Api())``  None
``GET``                          ``app.get(url)``                          ``requests.get(url)``
``POST``, ``PATCH`` (with JSON)  ``agg.post_json(url, data_dict)``         ``requests.post(url, json=data_dict)``
===============================  ========================================  ================

Please feel free to play with the demo server, currently hosted at
https://ojob.pythonanywhere.com/.


FAQ
---

Or « I don't get the message, why does it fail? » section.


``400 Bad Request: Cannot parse``
.................................

Briefly put: something in the request is not in expected format.

.. code-block:: python

  >>> my_app = backend.Api(init_project='My new Project')
  >>> app = webtest.TestApp(my_app)

Here's the example of a successful creation of a new transaction record:

.. code-block:: python

  >>> uid_1 = app.post_json(
  ...     '/api/v1/projects/my-new-project/users',
  ...     params=dict(name='Robert')).json['result']['users'][0]['uid']
  >>> uid_2 = app.post_json(
  ...     '/api/v1/projects/my-new-project/users',
  ...     params=dict(name='Robert')).json['result']['users'][0]['uid']
  >>> resp = app.post_json(
  ...     '/api/v1/projects/my-new-project/records',
  ...     params=dict(sender=uid_1,
  ...                 receivers=[uid_1, uid_2],
  ...                 amount=10.3))
  >>> resp.status
  '200 OK'
  >>> len(resp.json['result']['records'])
  1
  >>> rid = resp.json['result']['records'][0]['rid']


The app takes care to cast the values correctly, if in a close-enough type.
For instance, ``sender``, ``receivers`` and ``amount`` can be provided as
strings:

.. code-block:: python

  >>> resp = app.patch_json(
  ...     f'/api/v1/projects/my-new-project/records/rid={rid}',
  ...     params=dict(sender=str(uid_1),
  ...                 receivers=[uid_1, str(uid_2)],
  ...                 amount='10.30'))
  >>> resp.status
  '200 OK'
  >>> assert resp.json['result']['records'][0]['rid'] == rid
  >>> assert resp.json['result']['records'][0]['sender'] == uid_1  # int expected
  >>> assert resp.json['result']['records'][0]['receivers'] == [uid_1, uid_2]
  >>> assert resp.json['result']['records'][0]['amount'] == 10.3


But of course, value shall make sense. The ``message`` entry in the answer
provides some hints:

* ``sender`` shall be :py:class:`districounts.models.UserIdLike`:

  .. code-block:: python

    >>> resp = app.patch_json(
    ...     f'/api/v1/projects/my-new-project/records/rid={rid}',
    ...     params=dict(sender='one'),
    ...     expect_errors=True)
    >>> resp.status
    '400 Bad Request'
    >>> resp.json['message']
    "cannot parse 'sender': cannot execute UserId on 'one'"

    >>> resp = app.patch_json(
    ...   f'/api/v1/projects/my-new-project/records/rid={rid}',
    ...   params=dict(sender=''),
    ...   expect_errors=True)
    >>> resp.status
    '400 Bad Request'
    >>> resp.json['message']
    "cannot parse 'sender': cannot execute UserId on ''"

* ``receivers`` shall be *a non-empty list* of :py:class:`districounts.models.UserIdLike`:

  .. code-block:: python

    >>> resp = app.patch_json(
    ...     f'/api/v1/projects/my-new-project/records/rid={rid}',
    ...     params=dict(receivers=[]),
    ...     expect_errors=True)
    >>> resp.status
    '400 Bad Request'
    >>> resp.json['message']
    "cannot parse 'receivers': cannot execute UserId on '[]'"

    >>> resp = app.patch_json(
    ...     f'/api/v1/projects/my-new-project/records/rid={rid}',
    ...     params=dict(receivers=1),
    ...     expect_errors=True)
    >>> resp.status
    '400 Bad Request'
    >>> resp.json['message']
    "cannot parse 'receivers': cannot execute UserId on '1'"


  But beware of strings, that are indeed iterables:

  .. code-block:: python

    >>> my_app.projects['my-new-project'].records.users[1] = 'whatever'
    >>> my_app.projects['my-new-project'].records.users[2] = 'entries'
    >>> my_app.projects['my-new-project'].records.users[3] = 'to show the stuff just following'
    >>> resp = app.patch_json(
    ...     f'/api/v1/projects/my-new-project/records/rid={rid}',
    ...     params=dict(receivers='132'))  # iterable!
    >>> resp.status
    '200 OK'
    >>> resp.json['result']['records'][0]['receivers']
    [1, 2, 3]


``400 Bad Request: cannot import``
..................................

When importing data, it is expected that it is consistent. If it is not, the
import goes on until an inconsistency is found:

.. code-block:: python

  >>> my_app = backend.Api(init_project='My new Project')
  >>> app = webtest.TestApp(my_app)

  >>> from pathlib import Path
  >>> TEST_IN = Path(backend.__file__).parent.parent.parent / 'tests' / 'inputs'
  >>> json_inconsistent_path = TEST_IN / 'inconsistent-project-import.json'
  >>> with json_inconsistent_path.open('r') as json_fh:
  ...     json_inconsistent_data = json.load(json_fh)

  >>> resp = app.post_json(
  ...     '/api/v1/projects/my-new-project/import',
  ...     params=json_inconsistent_data,
  ...     expect_errors=True)
  >>> resp.status
  '400 Bad Request'
  >>> resp.json['message']
  'import failed: load failed: unknown sender uid: 13, unknown receivers uids: 14'


The other data, up to the failure, are already loaded though:

.. code-block:: python

  >>> len(app.get('/api/v1/projects/my-new-project/users').json['result']['users'])
  6
  >>> len(app.get('/api/v1/projects/my-new-project/records').json['result']['records'])
  1

.. Note:: imports come in addition to existing items, there is no wipe before
  import starts, and items with same ids are simply overwritten.

  This means that the import file can be modified just to fix the error, then
  reused completely for import again.



``400 Bad Request: cannot delete``
..................................

Once a user is involved in a record, it cannot be removed.

.. code-block:: python

  >>> my_app = backend.Api(init_project='My new Project')
  >>> app = webtest.TestApp(my_app)

  >>> resp = app.post_json(
  ...     '/api/v1/projects/my-new-project/users',
  ...     params=dict(name='bob'))
  >>> uid_bob: int = resp.json['result']['users'][0]['uid']
  >>> resp = app.post_json(
  ...     '/api/v1/projects/my-new-project/users',
  ...     params=dict(name='marina'))
  >>> uid_marina: int = resp.json['result']['users'][0]['uid']

  >>> resp = app.post_json(
  ...     '/api/v1/projects/my-new-project/records',
  ...     params=dict(sender=uid_bob,
  ...                 receivers=[uid_bob, uid_marina],
  ...                 amount=1))
  >>> assert resp.json['message'] == 'record created'
  >>> rid = resp.json['result']['records'][0]['rid']

  >>> resp = app.delete(
  ...     f'/api/v1/projects/my-new-project/users/uid={uid_bob}',
  ...     expect_errors=True)
  >>> resp.status
  '400 Bad Request'
  >>> resp.json['message'] == f'cannot delete user {uid_bob}: involved in {rid} as sender and receiver'
  True

  >>> resp = app.delete(
  ...     f'/api/v1/projects/my-new-project/users/uid={uid_marina}',
  ...     expect_errors=True)
  >>> resp.status
  '400 Bad Request'
  >>> resp.json['message'] == f'cannot delete user {uid_marina}: involved in {rid} as receiver'
  True
