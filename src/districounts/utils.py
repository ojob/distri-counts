# coding: utf-8
"""This module provides the back-end of districounts.

Under the hood, this is Bottle running: an `app` is created at module import,
together with routes and so on.

`POST` requests shall be accompanied by JSON data.
All responses are in JSON format.

"""

# ---- build-in imports ---------------------------------------------------
from __future__ import annotations

import collections
import dataclasses
import functools
import itertools
import logging
import typing
from decimal import Decimal
from json import JSONDecodeError

import bottle

from . import MONEYPREC


# ---- exceptions definition ----------------------------------------------
class ConversionError(Exception):
    """Exception to convey convertion errors."""


# ---- named tuples, types and similar ------------------------------------
ParseResult = collections.namedtuple("ParseResult", ['data', 'msg'])


# ---- decorators definition ----------------------------------------------
def log(
        logger: logging.Logger,
        level=logging.DEBUG,
) -> typing.Callable:
    """Log the call to the decorated function.

    Args:
        logger: logging instance to use for logging.
        level: level of the log message.

    Returns:
        decorated function

    """
    def func_wrapper(func: typing.Callable) -> typing.Callable:
        """Wrap *func* just by adding log call."""
        @functools.wraps(func)
        def wrapped_func(*args, **kwargs) -> typing.Any:
            logger.log(
                level=level,
                msg=("{} called with args:{}, kwargs: {})"
                     .format(func.__name__, args, kwargs)),
            )
            return func(*args, **kwargs)
        return wrapped_func
    return func_wrapper


# ---- decorators for inputs ----------------------------------------------
def ident_func(arg):
    """Identity function."""
    return arg


def func_or_none(func: typing.Callable) -> typing.Callable:
    """Decorate *func* so that it nicely handles `None`.

    Useful for instance to wrap an optional converter to `int` or `float`
    for instance.

    Raises:
        ConversionError: if *func* fails to run.

    >>> int_or_none = func_or_none(func=int)
    >>> [int_or_none(item) for item in ['1', 2, None]]
    [1, 2, None]

    >>> int_or_none('one')
    Traceback (most recent call last):
    ...
    districounts.utils.ConversionError: cannot execute int on 'one'

    """

    @functools.wraps(func)
    def new_func(arg):
        if arg is not None:
            try:
                return func(arg)
            except Exception as exc:
                raise ConversionError(f"cannot execute {new_func.__name__}"
                                      f" on '{arg}'") from exc
        return None
    return new_func


def str_to_seq(
        func: typing.Callable,
        sep: str = None,
) -> typing.Callable:
    """Decorate *func* to apply on split sequence of an input string.

    Args:
        func: converting function to apply to each item.
        sep: separator to use (see :py:func:`str.split` documentation)

    Examples of usage:

    >>> str_to_seq(func=seq_to_types(int), sep=';')('1;22')
    [1, 22]
    >>> str_to_seq(func=seq_to_types(ident_func))('1  32    24;13')
    ['1', '32', '24;13']

    Passing a value that cannot be split will raise an error:

    >>> str_to_seq(func=int, sep=';')(134)
    Traceback (most recent call last):
        ...
    districounts.utils.ConversionError: cannot split '134'

    """

    @functools.wraps(func)
    def new_func(str_seq: str):
        try:
            items = str_seq.split(sep=sep)
        except Exception as exc:
            raise ConversionError(f"cannot split '{str_seq}'") from exc
        return func(items)
    return new_func


def seq_to_types(
        func: typing.Callable,
        empty_ok: bool = False,
) -> typing.Callable:
    """Decorate *func* to apply on split sequence of an input string.

    Args:
        func: converting function to apply to each item.
        sep: separator to use (see :py:func:`str.split` documentation)

    Returns:
        function to return list of converted values.

    Examples of usage:

    >>> seq_to_types(func=int)(['123'])
    [123]
    >>> seq_to_types(func=int)(['12', '3'])
    [12, 3]

    >>> seq_to_types(func=int)([3, None])
    Traceback (most recent call last):
    ...
    districounts.utils.ConversionError: cannot execute int on 'None'

    >>> seq_to_types(func=int)(3)  # not a list and not iterable
    Traceback (most recent call last):
    ...
    districounts.utils.ConversionError: cannot iterate on '3'
    >>> seq_to_types(func=int)(3)  # not a list and not iterable
    Traceback (most recent call last):
    ...
    districounts.utils.ConversionError: cannot iterate on '3'

    By default, empty sequence raises a
    :py:class:`districounts.utils.ConversionError`, unless *empty_ok* is set
    to `True`:

    >>> seq_to_types(func=int)([])
    Traceback (most recent call last):
    ...
    districounts.utils.ConversionError: cannot execute int on empty list
    >>> seq_to_types(func=int, empty_ok=True)([])
    []

    """

    @functools.wraps(func)
    def new_func(seq: typing.Iterable):
        res = []
        if not empty_ok and not seq:
            raise ConversionError(f"cannot execute {new_func.__name__}"
                                  " on empty list")
        try:
            for item in seq:
                try:
                    res.append(func(item))
                except Exception as exc:
                    raise ConversionError(f"cannot execute {new_func.__name__}"
                                          f" on '{item}'") from exc
        except TypeError as exc:
            raise ConversionError(f"cannot iterate on '{seq}'") from exc
        return res
    return new_func


# ---- decorators ---------------------------------------------------------
def sanitize(
        request_attr: str,
        required_params: typing.Iterable[str] = None,
        requested_params: typing.Iterable[str] = None,
        converters: typing.Optional[typing.Dict[str, typing.Callable]] = None,
        failure_effect: str = 'break',
) -> typing.Callable:
    """Decorate an entry point, to check JSON format and required params.

    Args:
        request_attr: name of attribute of `bottle.request`, which attributes
            are expected to be those listed in either *required_params* or
            *requested_params*, most likely `json` or `forms`.
        required_params: sequence of mandatory parameters to find in
            the JSON payload.
        requested_params: sequence of parameters to pass to the decorated
            function.
        converters: mapping of converter functions to use for each parameter.
            If not defined, the value will be passed with no modification
            from the JSON payload.
        failure_effect: what to do in case of parsing failure. If "break",
            decorated function call is stopped, and answer to request is done
            with `status=400 Bad Request` and JSON answer. Otherwise, the
            call to the decorated function is performed with `msg` keyword.

    Returns:
        decorated function.

    """
    # Prepare arguments only once per decoration, to speed-up the routes
    # answers.
    if required_params is not None:  # sets needed for checks below
        required_params = set(required_params)
    else:
        required_params = set()
    if requested_params is not None:  # sets needed for checks below
        requested_params = set(requested_params)
    else:
        requested_params = set()
    if converters is None:
        converters = {}

    params = set(itertools.chain(required_params, requested_params))

    # define the decorated function
    def func_wrapper(func: typing.Callable) -> typing.Callable:
        @functools.wraps(func)
        def wrapped_func(*args, **kwargs):
            res = parse_request(
                request_attr=request_attr,
                params=params,
                required_params=required_params,
                converters=converters)
            if res.msg:
                if failure_effect == 'break':
                    # interrupt the function call, and answer to request
                    bottle.response.status = 400  # Bad Request
                    res = {'message': res.msg}
                elif failure_effect == 'pass-msg':
                    res = func(*args, **res.data, msg=res.msg, **kwargs)
                else:
                    bottle.response.status = 500
                    res = {'message': 'unknown failure_effect'}
            else:
                res = func(*args, **res.data, **kwargs)
            return res
        return wrapped_func
    return func_wrapper


def parse_request(
        request_attr: str,
        params: typing.Set[str],
        required_params: typing.Iterable[str],
        converters: typing.Optional[typing.Dict[str, typing.Callable]],
) -> ParseResult:
    """Parse `bottle.request` object to extract required and requested values.

    All arguments are mandatory, to avoid re-initialization from `None` object
    at each call of each route. For instance, this is already performed in
    :py:`sanitize`.

    Args:
        request_attr: name of attribute of `bottle.request`, which attributes
            are expected to be those listed in either *required_params* or
            *requested_params*, most likely `json` or `forms`.
        params: set of parameters names to extract.
        required_params: set of mandatory parameters to find in payload.
        converters: mapping of converter functions to use for each parameter.
            If not defined, the value will be passed with no modification
            from the JSON payload.

    Returns:
        parsed values; parsing messages.

    """
    res: ParseResult = None
    # get data and check input format
    try:
        source_data = getattr(bottle.request, request_attr)
    except JSONDecodeError as exc:
        # payload cannot be parsed as JSON
        res = ParseResult({}, f"wrong JSON data format: {exc}")
    if res is None and not source_data:
        res = ParseResult({}, f"empty request.{request_attr}")
    if res is None:
        # extract parameters values
        data, missing_params = {}, []
        for param_name in params:
            # extract value
            if param_name in required_params:
                val = source_data.get(param_name)
            else:
                val = source_data.get(param_name, None)
            # convert value
            try:
                data[param_name] = converters.get(param_name,
                                                  ident_func)(val)
            except ConversionError as exc:
                res = ParseResult({}, f"cannot parse '{param_name}': {exc}")
                break
            # check if some value was found
            if param_name in required_params and data[param_name] is None:
                missing_params.append(param_name)
        if res is None:
            if missing_params:
                res = ParseResult(
                    data={},
                    msg=("missing parameter(s): {}"
                         .format(', '.join(missing_params))))
            else:
                res = ParseResult(data=data, msg=None)
    return res


# --- decorator for outputs handling --------------------------------------
def api_versions(
        *version: typing.Text
) -> typing.Callable:
    """Add `version: *version_tag*` to the returned dict.

    Use as a decorator of REST API functions, so that the versions are
    conveniently added to the output, without even looking at the function
    body.

    Args:
        version: applicable version of the API. Several versions can be
            provided.

    Returns:
        decorator function

    """
    versions = list(version)

    def func_wrapper(func: typing.Callable) -> typing.Callable:
        """Wrap the *func* result to add entries in the return dict."""
        @functools.wraps(func)
        def wrapped_func(*args, **kwargs) -> typing.Dict:
            res = func(*args, **kwargs)
            res.update({'api_versions': versions})
            return res
        return wrapped_func
    return func_wrapper


# ---- decorators for output ----------------------------------------------
def set_res(**dec_kwargs) -> typing.Callable:
    """Decorate function to set default result values in return dict.

    Args:
        dec_kwargs: dictionary of default values.

    Returns:
        decorated function.

    >>> @set_res(c='ad info', d='other stuff')
    ... def some_func(a, b):
    ...     return {'a': a, 'c': b + a}
    ...
    >>> assert some_func(10, 20) == {'a': 10, 'c': 30, 'd': 'other stuff'}

    """
    def func_wrapper(func):
        @functools.wraps(func)
        def wrapped_func(*args, **kwargs):
            res = dec_kwargs.copy()
            res.update(func(*args, **kwargs))
            return res
        return wrapped_func
    return func_wrapper


# ---- functions ----------------------------------------------------------
def fmt_amount(amount, prec=MONEYPREC):
    """Ensure correct precision for transaction record amount.

    This ensures that precision of the returned :py:class:`decimal.Decimal`
    is at least two digits.

    >>> fmt_amount(10)
    Decimal('10.00')
    >>> fmt_amount('10.1342')
    Decimal('10.13')
    >>> fmt_amount(34.14)
    Decimal('34.14')
    >>> fmt_amount(34.1499)
    Decimal('34.15')

    """
    return Decimal(amount).quantize(prec)


def dataclass_from_dict(
        cls,
        data: dict,
        required: set = None,
        requested: set = None,
        exc_to_raise: Exception = KeyError,
):
    """Build a new instance of *cls* dataclass."""
    required = set(required) if required is not None else set()
    requested = set(requested) if requested is not None else set()
    for field in dataclasses.fields(cls):
        if (field.default == dataclasses.MISSING
                and field.default_factory == dataclasses.MISSING):
            required.add(field.name)
        else:
            requested.add(field.name)
    attributes = {}
    missing_attr = set()
    for attr_name in required.union(requested):
        try:
            attributes[attr_name] = data[attr_name]
        except KeyError:
            if attr_name in required:
                missing_attr.add(attr_name)
    if missing_attr:
        raise exc_to_raise("field(s) missing: "
                           + ", ".join(sorted(missing_attr)))
    return cls(**attributes)
