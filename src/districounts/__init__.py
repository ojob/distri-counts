# coding: utf-8
# #########################################################################
# Copyright (C) 2020 Joël BOURGAULT
#
# You can contact me at following address: joel.bourgault@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or any later
# version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Non-official translations are available here:
# <https://www.gnu.org/licenses/translations.html>
#
# #########################################################################
"""This module provides the constants for the whole package."""

# ---- build-in imports ---------------------------------------------------
import logging
import pkgutil
from decimal import Decimal

# ---- third-party imports ------------------------------------------------

# ---- local imports ------------------------------------------------------

# ---- package metadata ---------------------------------------------------
#: author of the package
__author__ = "Joël Bourgault"

#: package resources
LINKS = {
    'homepage': 'https://framagit.org/ojob/districounts',
    'documentation': 'https://ojob.frama.io/districounts/',
    'bugtracker': 'https://framagit.org/ojob/districounts/-/issues',
    'roadmap': 'https://framagit.org/ojob/districounts/-/milestones',
    }

VERSION_FN = 'VERSION'  #: filename providing the package version
try:
    # pkgutil.get_data output is a byte stream, that needs decoding
    RAW_VERSION = pkgutil.get_data(__name__, VERSION_FN)
except FileNotFoundError:
    logging.error("Cannot retrieve version: %s", exc_info=True)
    RAW_VERSION = None

if RAW_VERSION is None:
    #: version of the package
    __version__ = '0.0.0'
    __version_short__ = '0.0'  #: version of the package, short way
else:
    __version__ = RAW_VERSION.decode()
    # sanitize string by removing blank characters
    for char in ' \r\n\t':
        __version__ = __version__.replace(char, '')
    # short version: only the two first components
    __version_short__ = '.'.join(__version__.split('.')[:2])


# ---- constants definition -----------------------------------------------
#: verbosity levels
VERBOSITY = {0: logging.WARNING, 1: logging.INFO, 2: logging.DEBUG}

HOST: str = 'localhost'  #: default host location
PORT: int = 8080  #: default port to serve

#: Precision required for money amounts
MONEYPREC = Decimal('0.01')
