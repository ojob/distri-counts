# coding: utf-8
"""This module provides the back-end of districounts.

Under the hood, this is Bottle running: an `app` is created at module import,
together with routes and so on.

`POST` requests shall be accompanied by JSON data.
All responses are in JSON format.

"""

# ---- build-in imports ---------------------------------------------------
from __future__ import annotations

import functools
import inspect
import logging
import typing
from decimal import Decimal
from pathlib import Path

# ---- third-party imports ------------------------------------------------
import bottle

# ---- local imports ------------------------------------------------------
from . import LINKS, __version__
from . import models, utils

# ---- constants definition -----------------------------------------------
#: converter for inputs towards `RecordStore` instances
USER_CONVERTERS = {
    'uid': utils.func_or_none(models.UserId)
    }
API_RECORD_CONVERTERS = {
    'rid': utils.func_or_none(models.RecordId),
    'sender': utils.func_or_none(models.UserId),
    'receivers': utils.func_or_none(utils.seq_to_types(models.UserId)),
    'amount': utils.func_or_none(utils.fmt_amount)
    }
WEB_RECORD_CONVERTERS = {
    'rid': utils.func_or_none(models.RecordId),
    'sender': utils.func_or_none(models.UserId),
    'receivers': utils.func_or_none(
        utils.str_to_seq(utils.seq_to_types(models.UserId))),
    'amount': utils.func_or_none(utils.fmt_amount)
    }

# add absolute path towards views folder
bottle.TEMPLATE_PATH.insert(0, Path(__file__).parent / 'views')

# ---- exceptions definition ----------------------------------------------


# ---- decorators definition ----------------------------------------------
def route(
        path: str,
        verb: str,
) -> typing.Callable:
    """Decorate function by setting (path, verb) as *route* attribute.

    This will then enable :py:meth:`App.set_routes` to bind these routes as
    API entry points.

    >>> @route(path='blabla', verb='GET')
    ... def f():
    ...    pass
    ...
    >>> f.routes
    {'blabla': 'GET'}

    """
    def func_wrapper(func):
        assert func is not None
        if not hasattr(func, 'routes'):
            func.routes = {}
        func.routes[path] = verb
        return func
    return func_wrapper


def get_project_from_slug(
        failure_effect: str = 'break',
) -> typing.Callable:
    """Decorate a method callback, to provide it with project instance.

    The class instance to which the method is bound shall provide a
    `projects` attribute at runtime, that is a :py:class:`models.Projects`
    instance.

    Use this as a decorator *after* app routing decoration, so that the
    ``<slug>`` of the route is converted to a ``project`` argument, with
    correct handling of failure to get the project.

    Args:
        failure_effect: defines the sanction in case the *slug* cannot be
            found:

            * 'break': stop execution, and return bare status 404 with
              error description
            * 'err-404': call normal Error 404 callback

    >>> class A:
    ...     @get_project_from_slug(failure_effect='break')
    ...     def compute(self, project):
    ...         return f"project name: {project.name}"
    ...
    >>> a = A()
    >>> a.projects = models.Projects()
    >>> _ = a.projects.create_project('a <> project')
    >>> a.compute(slug='a-project')
    'project name: a <> project'
    >>> a.compute(slug='another-one')
    {'message': "project not found: unknown slug: 'another-one'", 'result': {}}

    >>> class B:
    ...     @get_project_from_slug(failure_effect='err-404')
    ...     def compute(self, project):
    ...         return f"project name: {project.name}"
    ...
    ...     def error_404(self, error):
    ...         return f"error 404: {error}"
    >>> b = B()
    >>> b.projects = a.projects
    >>> b.compute(slug='---')
    "error 404: project not found: unknown slug: '---'"

    >>> class C:
    ...     @get_project_from_slug(failure_effect='ah ah !')
    ...     def compute(self, project):
    ...         return f"project name: {project.name}"
    ...
    >>> c = C()
    >>> c.projects = a.projects
    >>> c.compute(slug='---')
    {'message': 'unknown failure_effect'}

    """
    def func_wrapper(func: typing.Callable) -> typing.Callable:
        @functools.wraps(func)
        def wrapped_func(self, *args, slug, **kwargs):
            try:
                project = self.projects.get_project(slug=slug)
            except models.ProjectAccessError as exc:
                msg = f"project not found: {exc}"
                if failure_effect == 'break':
                    bottle.response.status = 404  # not found
                    res = {'message': msg,
                           'result': {}}
                elif failure_effect == 'err-404':
                    res = self.error_404(error=msg)
                else:
                    bottle.response.status = 500
                    res = {'message': 'unknown failure_effect'}
            else:
                res = func(self, *args, project=project, **kwargs)
            return res
        return wrapped_func
    return func_wrapper


def add_metadata(meth):
    """Decorate method to add metadata to its output, as dictionary.

    Metadata are retrieved from its class attribute `metadata`, that shall
    be a dictionary.

    >>> class A:
    ...     def __init__(self):
    ...         self.data = 'cheerio'
    ...
    ...     @property
    ...     def metadata(self):
    ...         return {'data': self.data}
    ...
    ...     @add_metadata
    ...     def compute(self):
    ...         return {'result': 'some result'}
    ...
    >>> a = A()
    >>> a.compute()
    {'result': 'some result', 'data': 'cheerio'}

    """
    @functools.wraps(meth)
    def wrapped_meth(self, *args, **kwargs):
        res = meth(self, *args, **kwargs)
        for key, val in self.metadata.items():
            res.setdefault(key, val)
        return res
    return wrapped_meth


# ---- Bottle instantiation -----------------------------------------------
class BottleApp(bottle.Bottle):
    """Sub-class from Bottle, to add specific utilities.

    To use this class in an interesting way:

    * use `@route` decorator on related methods;
    * set `self.metadata` to some relevant values.

    """

    def __init__(self, *args, **kwargs):
        """Initialize."""
        super().__init__(*args, **kwargs)
        self.set_routes()

    def set_routes(self):
        """Set the routes on all methods declared so.

        Simply decorate the methods with `@route` decorator from this module.

        """
        routes = [
            (path, verb, method)
            for meth_name, method in inspect.getmembers(
                self, predicate=inspect.ismethod)
            if hasattr(method, 'routes')
            for path, verb in method.routes.items()]

        # call bottle.Bottle route setting
        logging.info("%s routes set", len(routes))
        for path, verb, method in routes:
            logging.debug("- %s %s -> %s", path, verb, method)
            if verb == 'error':
                try:
                    # bottle syntax for versions >= 0.13
                    # pylint: disable=unexpected-keyword-arg
                    self.error(code=path, callback=method)
                except TypeError:
                    # for 'old' bottle versions, i.e. <=0.12
                    self.error(code=path)(handler=method)
            elif verb == 'hook':
                self.add_hook(name=path, func=method)
            else:
                self.route(path=path, method=verb, callback=method)


# pylint: disable=too-many-public-methods
class Api(BottleApp):
    """Create new application server for Districounts.

    This is a Bottle instance, ready to be launched on a WSGI server, to
    provide the REST API.

    It always return a JSON-formatted text. The version of the API is part
    of the called URL.

    This API is a wrapper as thin as possible on the content of
    :py:mod:`.models` content.

    Result dict contains following keys:

    - `app_version`: version of `districounts`, as string;
    - `message`: some comment about the request answer, as string;
    - `result`: answer to the request, providing expected payload as dict.

    Entry points:

    - `/api/v1/healthy`: provides healthiness status.
    - '/api/v1/links': provides resource links.
    - '/api/v1/projects': provides list of projects.
    - '/api/v1/projects/<slug>': provides detail of project identified by
      its slug.
    - `/api/v1/projects/<slug>/users`: list of users in this project
    - `/api/v1/projects/<slug>/users/balance`: balance of users in this project
    - `/api/v1/projects/<slug>/users/uid=<uid>`: information about given user
    - `/api/v1/projects/<slug>/users/name=<name>`: list of users matching
      `<name>`
    - `/api/v1/projects/<slug>/records`: transactions in this project
    - `/api/v1/projects/<slug>/balance`: balance of the project

    The version is 1 for the moment.

    .. Note:: version bumps will occurr when non-backward-compatibles
      changes are made, and new routes will be attached to new functions.

      Some fuctions will however still work; these shall be decorated
      with a route that accepts different versions (i.e. something like
      `@app.route('/api/v<api_version:int>/XXX')`.

    """

    def __init__(
            self,
            debug: bool = False,
            json_data: dict = None,
            create_sample: bool = False,
            init_project: str = None,
    ):
        """Initialize.

        Args:
            debug: enable debugging.
            json_data: input dict for models initialization.
            create_sample: request underlying models to create some sample
                items. This argument is ignored if *json_data* is set.
            init_project: if set, is used to create an initial, empty project.

        """
        super().__init__()

        self.logger = logging.getLogger('backend')
        self.debug = debug
        self.projects = models.Projects(json_data=json_data)
        self.metadata = {
            'app_version': __version__,
            'mode': 'development' if self.debug else 'production',
            }
        if self.debug:
            bottle.debug(True)

        if json_data is None or not json_data:
            if init_project is not None:
                project = self.projects.create_project(name=init_project)
            if create_sample:
                if init_project is None:
                    project = self.projects.create_project(
                        name="Sample Project")
                project.create_sample()

    # ---- routes modifiers and manipulations -----------------------------
    @route(path='before_request', verb='hook')
    @staticmethod
    def strip_path():
        """Manipulate request to remove trailing slash."""
        bottle.request.environ['PATH_INFO'] = \
            bottle.request.environ['PATH_INFO'].rstrip('/')

    # ---- tweaking ansnwers ----------------------------------------------
    # TODO: remove this when authentication will be working
    @route(path='after_request', verb='hook')
    @staticmethod
    def enable_cors():
        """Enable CORS header, for Cross-Origin-Request Service."""
        bottle.response.headers['Access-Control-Allow-Origin'] = '*'

    # ---- REST API entry points ------------------------------------------
    @route(path='/api/v1/healthy', verb='GET')
    @add_metadata
    def healthy(
            self,
    ) -> typing.Dict[str, typing.Any]:
        """Return healthiness status."""
        return {
            'result': {'healthy': True},
            'app_version': __version__,
            'mode': 'development' if self.debug else 'production',
            }

    # pylint: disable=no-self-use  # necessary for @add_metadata call
    @route(path='/api/v1/links', verb='GET')
    @add_metadata
    def links(
            self,
    ) -> typing.Dict[str, typing.Any]:
        """Return healthiness status."""
        return {'result': LINKS}

    # ---- projects ----
    @route(path='/api/v1/projects', verb='GET')
    @add_metadata
    def list_projects(
            self,
    ) -> typing.Dict[str, typing.Any]:
        """Return all projects, as a list."""
        return {'result': {'projects': self.projects.projects_as_list()}}

    @route(path='/api/v1/projects', verb='POST')
    @add_metadata
    @utils.sanitize(request_attr='json',
                    required_params=['name'])
    def create_project(
            self,
            name: str = None,
    ) -> typing.Dict[str, typing.Any]:
        """Create new project from POST request, formatted as JSON.

        Args:
            name: name of the new project to create.

        """
        try:
            project: models.SharesProject = \
                self.projects.create_project(name=name)
        except models.ProjectCreateError as exc:
            bottle.response.status = 400
            return {'message': f'cannot create project: {exc}'}
        # return identification last created user
        return {'result': {'projects': [project.as_dict()]},
                'message': 'project created'}

    @route(path='/api/v1/projects/<slug>', verb='PATCH')
    @get_project_from_slug()
    @utils.sanitize(request_attr='json',
                    required_params=['name'])
    @add_metadata
    def update_project(
            self,
            project: models.SharesProject,
            name: str,
    ) -> typing.Dict[str, typing.Any]:
        """Update project from PATCH request, formatted as JSON.

        Args:
            name: new name of the project to update.

        """
        try:
            new_slug = self.projects.update_project(slug=project.slug,
                                                    new_name=name)
        except models.ProjectUpdateError as exc:
            bottle.response.status = 400
            return {'message': f'cannot update: {exc}'}
        # and retrieve the updated project
        project = self.projects.get_project(slug=new_slug).as_dict()
        return {'result': {'projects': [project]},
                'message': 'project updated'}

    @route(path='/api/v1/projects/<slug>', verb='delete')
    @get_project_from_slug()
    @add_metadata
    def delete_project(
            self,
            project: models.SharesProject,
    ) -> typing.Dict[str, typing.Any]:
        """Delete project from DELETE request, formatted as JSON.

        Args:
            name: name of the project to delete.

        """
        self.projects.delete_project(slug=project.slug)
        # TODO: handle case where models.ProjectDeleteError would be raised
        return {'result': {'projects': [project.as_dict()]},
                'message': 'project deleted'}

    # pylint: disable=no-self-use  # necessary for @add_metadata call
    @route(path='/api/v1/projects/<slug>', verb='GET')
    @get_project_from_slug()
    @add_metadata
    def project_by_slug(
            self,
            project: models.SharesProject,
    ) -> typing.Dict[str, typing.Any]:
        """Return a project by its *slug*."""
        return {'result': {'projects': [project.as_dict()]}}

    # ---- users ----
    # pylint: disable=no-self-use  # necessary for @add_metadata call
    @route(path='/api/v1/projects/<slug>/users', verb='GET')
    @get_project_from_slug()
    @add_metadata
    def list_users(
            self,
            project: models.SharesProject,
    ) -> typing.Dict[str, typing.Any]:
        """Return all users as a list."""
        return {'result': {'users': project.users_as_list()}}

    # pylint: disable=no-self-use  # necessary for @add_metadata call
    @route(path='/api/v1/projects/<slug>/users/name=<name>', verb='GET')
    @get_project_from_slug()
    @add_metadata
    def list_users_by_name(
            self,
            project: models.SharesProject,
            name: str,
    ) -> typing.Dict[str, typing.Any]:
        """Return users by *name*."""
        try:
            users = project.users[name]
        except KeyError:
            bottle.response.status = 404  # not found
            return {'message': f"user name not found: '{name}'"}
        return {'result': {'users': [user.as_dict() for user in users]}}

    # pylint: disable=no-self-use  # necessary for @add_metadata call
    @route(path='/api/v1/projects/<slug>/users/uid=<uid_raw:int>', verb='GET')
    @get_project_from_slug()
    @add_metadata
    def show_user(
            self,
            project: models.SharesProject,
            uid_raw: int,
    ) -> typing.Dict[str, typing.Any]:
        """Return user by *uid_raw*."""
        uid = models.UserId(uid_raw)
        try:
            user = project.users[uid]
        except KeyError:
            bottle.response.status = 404  # not found
            return {'message': f"user id not found: {uid}"}
        return {'result': {'users': [user.as_dict()]}}

    # pylint: disable=no-self-use  # necessary for @add_metadata call
    @route(path='/api/v1/projects/<slug>/users', verb='POST')
    @get_project_from_slug()
    @add_metadata
    @utils.sanitize(request_attr='json',
                    required_params=['name'],
                    requested_params=['email'])
    def create_user(
            self,
            project: models.SharesProject,
            name: str,
            email: str = None,
    ) -> typing.Dict[str, typing.Any]:
        """Create new user from POST request, formatted as JSON.

        Args:
            name: name of the new user to create
            email: email of the new user to create, optional.

        """
        new_uid: models.UserId = project.create_user(name=name, email=email)
        # return identification last created user
        return {'result': {'users': [project.users[new_uid].as_dict()]},
                'message': 'User created'}

    # pylint: disable=no-self-use  # necessary for @add_metadata call
    @route(path='/api/v1/projects/<slug>/users/uid=<uid_raw:int>',
           verb='PATCH')
    @get_project_from_slug()
    @utils.sanitize(request_attr='json',
                    requested_params=['name', 'email'])
    @add_metadata
    def update_user(
            self,
            project: models.SharesProject,
            uid_raw: int,
            name: str = None,
            email: str = None,
    ) -> typing.Dict[str, typing.Any]:
        """Update user characteristics from PATCH request, formatted as JSON.

        Args:
            uid_raw: identifier of the user to update, as int.
            name: if provided, new name
            email: if provided, new email

        """
        uid = models.UserId(uid_raw)
        user = project.update_user(uid=uid, name=name, email=email)
        return {'result': {'users': [user.as_dict()]},
                'message': f'user {uid} updated'}

    # pylint: disable=no-self-use  # necessary for @add_metadata call
    @route(path='/api/v1/projects/<slug>/users/uid=<uid_raw:int>',
           verb='DELETE')
    @get_project_from_slug()
    @add_metadata
    def delete_user(
            self,
            project: models.SharesProject,
            uid_raw: int,
    ) -> typing.Dict[str, typing.Any]:
        """Delete user bearing *uid_raw*."""
        uid = models.UserId(uid_raw)
        try:
            user = project.delete_user(uid=uid)
        except models.UserDeleteError as exc:
            bottle.response.status = 400
            return {'message': f'cannot delete user {uid}: {exc}'}
        return {'result': {'users': [user.as_dict()]},
                'message': f'user {uid} deleted'}

    # ---- records ----
    @route(path='/api/v1/projects/<slug>/records', verb='GET')
    @get_project_from_slug()
    @add_metadata
    def list_records(
            self,
            project: models.SharesProject,
    ) -> typing.Dict[str, typing.Any]:
        """Return all transaction records."""
        return {'result': {'records': project.records_as_list()}}

    # pylint: disable=no-self-use  # necessary for @add_metadata call
    @route(path='/api/v1/projects/<slug>/records/rid=<rid_raw>', verb='GET')
    @get_project_from_slug()
    @add_metadata
    def show_record(
            self,
            project: models.SharesProject,
            rid_raw: str,
    ) -> typing.Dict[str, typing.Any]:
        """Return records bearing *rid_raw*."""
        try:
            rid = models.RecordId(rid_raw)
            record = project.records[rid]
        except models.RecordAccessError as exc:
            bottle.response.status = 404  # Not Found
            records = []
            msg = str(exc)
        else:
            records = [record.as_dict()]
            msg = 'Record found'
        return {'result': {'records': records}, 'message': msg}

    # pylint: disable=no-self-use  # necessary for @add_metadata call
    @route(path='/api/v1/projects/<slug>/records', verb='POST')
    @get_project_from_slug()
    @add_metadata
    @utils.sanitize(request_attr='json',
                    required_params=['sender', 'receivers', 'amount'],
                    converters=API_RECORD_CONVERTERS)
    def create_record(
            self,
            project: models.SharesProject,
            sender: models.UserId,
            receivers: typing.List[models.UserId],
            amount: Decimal,
    ) -> typing.Dict[str, typing.Any]:
        """Create a new transaction record.

        Args:
            sender: ID of the user that paid the transaction
            receivers: list of user IDs that benefitiated from the
                transactions.
            amount: amount of the transaction.

        """
        try:
            rid = project.create_record(
                sender=sender, receivers=receivers, amount=amount)
        except models.RecordCreateError as exc:
            bottle.response.status = 400  # Bad Request
            return {'message': f"cannot create record: {exc}"}

        # and if all went well
        return {'result': {'records': [{'rid': str(rid)}]},
                'message': "record created"}

    # pylint: disable=no-self-use  # necessary for @add_metadata call
    @route(path='/api/v1/projects/<slug>/records/rid=<rid_raw>', verb='PATCH')
    @get_project_from_slug()
    @add_metadata
    @utils.sanitize(request_attr='json',
                    requested_params=['sender', 'receivers', 'amount'],
                    converters=API_RECORD_CONVERTERS)
    def update_record(
            self,
            project: models.SharesProject,
            rid_raw: str,
            sender: models.UserId = None,
            receivers: typing.List[models.UserId] = None,
            amount: Decimal = None,
    ) -> typing.Dict[str, typing.Any]:
        """Update transaction record.

        Args:
            rid_raw: ID of the record to update, string format.
            sender: if provided, the new sender id.
            receivers: if provided, the new receivers ids list.
            amount: if provided, the new amout.

        Returns:
            dict with updated record object.

        """
        rid = models.RecordId(rid_raw)
        try:
            record = project.update_record(
                rid=rid, sender=sender, receivers=receivers, amount=amount)
        except models.RecordUpdateError as exc:
            bottle.response.status = 400
            return {'message': f"cannot update record: {exc}"}

        return {'result': {'records': [record.as_dict()]},
                'message': f'record {rid} updated'}

    # pylint: disable=no-self-use  # necessary for @add_metadata call
    @route(path='/api/v1/projects/<slug>/records/rid=<rid_raw>', verb='DELETE')
    @get_project_from_slug()
    @add_metadata
    def delete_record(
            self,
            project: models.SharesProject,
            rid_raw: typing.Text,
    ) -> typing.Dict[str, typing.Any]:
        """Delete transaction record.

        Args:
            rid_raw: string representation of the record to update.

        Returns:
            dict with deleted record object.

        """
        try:
            rid = models.RecordId(rid_raw)
            record = project.delete_record(rid=rid)
        except models.RecordAccessError as exc:
            bottle.response.status = 404  # Not Found
            records, msg = [], str(exc)
        else:
            records = [record.as_dict()]
            msg = f'record {rid} deleted'
        return {'result': {'records': records}, 'message': msg}

    # pylint: disable=no-self-use  # necessary for @add_metadata call
    @route(path='/api/v1/projects/<slug>/users/balance', verb='GET')
    @get_project_from_slug()
    @add_metadata
    def users_balance(
            self,
            project: models.SharesProject,
    ) -> typing.Dict[str, typing.Any]:
        """List the current users' balance."""
        return {'result': {'users_balance': [
            {'uid': uid, 'user_balance': round(float(user_balance), ndigits=2)}
            for uid, user_balance
            in project.users_balance().items()]}}

    # pylint: disable=no-self-use  # necessary for @add_metadata call
    @route(path='/api/v1/projects/<slug>/balance', verb='GET')
    @get_project_from_slug()
    @add_metadata
    def project_balance(
            self,
            project: models.SharesProject,
    ) -> typing.Dict[str, typing.Any]:
        """List the project flows for balance."""
        return {'result': {'balance': [
            [sender, receiver, round(float(balance_flow), ndigits=2)]
            for sender, receiver, balance_flow
            in project.balance()]}}

    # ---- I/O ------------------------------------------------------------
    # pylint: disable=no-self-use  # necessary for @add_metadata call
    @route(path='/api/v1/import', verb='POST')
    @utils.sanitize(request_attr='json',
                    requested_params=['projects'])
    @add_metadata
    def import_(  # import is reserved keyword
            self,
            projects: dict = None,
    ) -> typing.Dict[str, typing.Any]:
        """Import data from request to stored data."""
        try:
            load_msg = self.projects.load({'projects': projects})
        except models.LoadError as exc:
            bottle.response.status = 400
            return {'message': f"import failed: {exc}"}
        return {'message': f"import successfull: {load_msg}"}

    # pylint: disable=no-self-use  # necessary for @add_metadata call
    @route(path='/api/v1/projects/<slug>/import', verb='POST')
    @get_project_from_slug()
    @utils.sanitize(request_attr='json',
                    requested_params=['users', 'records'])
    @add_metadata
    def project_import(
            self,
            project: models.SharesProject,
            users: dict = None,
            records: dict = None,
    ) -> typing.Dict[str, typing.Any]:
        """Import data from request to stored data."""
        try:
            load_msg = project.load(
                {'users': users, 'records': records})
        except models.LoadError as exc:
            bottle.response.status = 400
            return {'message': f"import failed: {exc}"}
        return {'message': f"import successfull: {load_msg}"}

    @route(path='/api/v1/export', verb='GET')
    @add_metadata
    def export(
            self,
    ) -> typing.Dict[str, typing.Any]:
        """Export the whole content as dict, ready for JSON dump."""
        return {'result': self.projects.dump(),
                'message': 'export successfull'}

    # ---- catching erroneous requests ------------------------------------
    # pylint: disable=no-self-use  # necessary for @add_metadata call
    @route(path=404, verb='error')
    @bottle.view('error-404')
    @add_metadata
    def error_404(
            self,
            error,
    ) -> typing.Dict[str, typing.Any]:
        """Return error page."""
        return {'error': error}
