# coding: utf-8
"""Module providing command-line interface."""

# ---- build-in imports ---------------------------------------------------
import json
import logging

# ---- third-party imports ------------------------------------------------
import bottle

import click

# ---- local imports ------------------------------------------------------
from . import HOST, PORT, VERBOSITY, __version__
from . import backend, frontend


# ---- CLI definition -----------------------------------------------------
@click.group()
@click.option('-d', '--debug', is_flag=True,
              help=("Activate debug mode."))
@click.option('--create-sample', 'create_sample', is_flag=True,
              help=("Create sample data in models."))
@click.option('-v', '--verbose', count=True, default=0,
              help=("Increase verbosity; default is to display WARNING "
                    "messages and higher. Set once, displays also INFO "
                    "messages; repeated, displays also DEBUG messages."))
@click.version_option()
@click.pass_context
def cli(
        ctx: dict,
        debug: bool = False,
        create_sample: bool = False,
        verbose: int = 0,
) -> int:
    """Handle command-line interface."""
    logging.basicConfig(level=VERBOSITY[verbose])
    logging.debug("started execution %s- version %s",
                  " in debug mode" if debug else "",
                  __version__)

    ctx.ensure_object(dict)
    ctx.obj['debug'] = debug
    ctx.obj['create_sample'] = create_sample

    return 0


@cli.command()
@click.option('-h', '--host', default=HOST,
              help=("address to use"))
@click.option('-p', '--port', default=PORT,
              help=("port number to use"))
@click.option('--with-html/--no-html', 'with_html', default=True,
              help=("Trigger activation of HTML front-end."))
@click.option('-i', '--init-from', 'init_from',
              type=click.Path(exists=True, dir_okay=False, readable=True),
              help=("Path to a JSON file, to be imported in the app."))
@click.option('-r', '--reloader', is_flag=True,
              help=("reload the server upon file change"))
@click.pass_context
def serve(
        ctx: dict,
        host: str,
        port: int,
        with_html: bool = True,
        init_from: str = None,
        reloader: bool = False,
) -> None:
    """Command to manage the server.

    By default, starts the server and blocks the terminal.

    """
    test_run(host=host, port=port,
             with_html=with_html,
             debug=ctx.obj['debug'],
             init_from=init_from,
             create_sample=ctx.obj['create_sample'],
             reloader=reloader)


# ---- launching the server -----------------------------------------------
def create_app(
        with_html: bool = True,
        json_data: dict = None,
        create_sample: bool = False,
        debug: bool = False,
) -> bottle.Bottle:
    """Create application to be launched on WSGI server."""
    api_app = backend.Api(
        debug=debug, json_data=json_data, create_sample=create_sample)
    if with_html:
        html_frontend = frontend.HtmlFrontend(api_app=api_app)
        api_app.merge(html_frontend)

    return api_app


def test_run(
        host: str = HOST,
        port: int = PORT,
        with_html: bool = True,
        init_from: str = None,
        create_sample: bool = False,
        debug: bool = False,
        **kwargs: dict,
) -> None:
    """Start a development server.

    Args:
        host: IP to serve; defaults to `localhost`
        port: port to serve; default is 8080
        with_html: whether to start the HTML front-end or not.
        init_from: path to a JSON file, providing initialization data.
        create_sample: request underlying models to create some sample items.
            This option is ignored if *init_from* is provided.
        debug: serve in debug mode, creating default samples and others.
        kwargs: additional parameters, passed to `bottle.run`.

    """
    bottle.debug(mode=debug)
    # initialize the ap
    if init_from is not None:
        with open(init_from) as json_fh:
            json_data = json.load(json_fh)
    else:
        json_data = None

    app = create_app(
        with_html=with_html,
        json_data=json_data, create_sample=create_sample,
        debug=debug)

    # and launch the server
    bottle.run(app=app,
               host=host, port=port,
               **kwargs)
