%rebase('base.tpl')
<p>Welcome to this counts and transactions follow-up web-app!</p>
%if result['projects']:
<p>Here's the list of projects in this instance:</p>
<ul>
    %for project in result['projects']:
    <li><a href="/projects/{{ project['slug'] }}">{{ project['name'] }}</a>
        <form action="/projects" method="post">
            <input name="action" value="delete" hidden="true" />
            <input name="slug" value="{{ project['slug'] }}" hidden="true" />
            <input value="X" type="submit" />
        </form>
    </li>
    %end
</ul>
%else:
<p>No project in this instance.</p>
%end
<form action="/projects" method="post">
    <input name="action" value="create" hidden="true" />
    Name: <input name="name" type="text" />
    <input value="create" type="submit" />
</form>
