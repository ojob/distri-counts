%rebase('base.tpl')
%if defined('result') and result['records']:
%    record = result['records'][0]
<form action="/projects/{{ project['slug'] }}/records/rid={{ record['rid'] }}" method="post">
    <input name="action" value="update" hidden="true" />
    <input name="rid" value="{{ record['rid'] }}" hidden="true" />
    Sender: <input name="sender" type="text" value="{{ record['sender'] }}"/>
    Receivers: <input name="receivers" type="text" value="{{ ' '.join(str(rec) for rec in record['receivers']) }}"/>
    Amount: <input name="amount" type="text" value="{{ record['amount'] }}"/>
    <input value="update" type="submit" />
</form>
%else:
<p>No record to display!</p>
%end
