<html>
<head>
    %if defined('title'):
    %   page_title = title
    %elif defined('project'):
    %   page_title = project['name']
    %else:
    %   page_title = 'Districounts'
    %end

    <title>{{ page_title }}</title>
    <style type="text/css">
        .notice {background-color: blue;}
        .warning {background-color: orange;}
        .error {background-color: red;}
    </style>
</head>
<body>
    <div id="content">
        <h1>{{page_title}}</h1>
        %if defined('msg'):
            <p class="{{msg_type}}">{{msg}}</p>
        %end
        {{!base}}
    </div>
    <div id="nav">
        <p>Pages:</p>
        <ul>
            <li><a href="/home">home</a></li>
            <li><a href="/projects">projects</a></li>

            %if defined('project'):
            <li><a href="/projects/{{ project['slug'] }}">{{ project['name'] }} content:</a></li>
            <ul>
                <li><a href="/projects/{{ project['slug'] }}/users">users</a></li>
                <li><a href="/projects/{{ project['slug'] }}/records">records</a></li>
                <li><a href="/projects/{{ project['slug'] }}/users/balance">users balance</a></li>
                <li><a href="/projects/{{ project['slug'] }}/balance">project balance</a></li>
            </ul>
            %end
        </ul>
    </div>
    <div id="footer">
        %if defined('app_version') and defined('mode'):
        <p>version: {{app_version}} / running mode: {{mode}}</p>
        %end
        %if defined('links'):
        <ul>
            %for page, link in links.items():
            <li><a href="{{ link }}">districounts {{ page }}</a></li>
            %end
        </ul>
        %end
    </div>
</body>
</html>
