%rebase('base.tpl')
%if result['users_balance']:
<p>Here's the balance of each user in this project:</p>
<ul>
    %for balance in result['users_balance']:
    <li>{{balance['uid']}}: {{'{:+.2f}'.format(balance['user_balance'])}}</li>
    %end
</ul>
%else:
<p>No users in this instance.</p>
%end
