%rebase('base.tpl')
%if 'users' in result and result['users']:
<p>Here's the list of users in this instance:</p>
<ul>
    %for user in result['users']:
    <li>
        <a href="/projects/{{ project['slug'] }}/users/uid={{user['uid']}}">user {{ user['uid'] }}</a>:
        {{ user['name'] }}</a>
        <form action="/projects/{{ project['slug'] }}/users" method="post">
            <input name="uid" value="{{ user['uid' ]}}" hidden="true" />
            <input name="name" value="{{ user['name' ]}}" hidden="true" />
            <input name="action" value="delete" hidden="true" />
            <input value="X" type="submit" />
        </form>
    </li>
    %end
</ul>
%else:
<p>No users in this instance.</p>
%end
<form action="/projects/{{ project['slug'] }}/users" method="post">
    <input name="action" value="create" hidden="true" />
    Name: <input name="name" type="text" />
    Email: <input name="email" type="text" />
    <input value="create" type="submit" />
</form>
