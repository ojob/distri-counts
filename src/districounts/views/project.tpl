%rebase('base.tpl')
<form action="/projects/{{ project['slug'] }}" method="post">
    <input name="action" value="update" hidden="true" />
    Name: <input name="name" type="text" value="{{ project['name'] }}"/>
    <input value="update" type="submit" />
</form>
<p>Currently stored in project {{ project['name'] }}:</p>
<ul>
    <li>number of <a href='/projects/{{ project['slug'] }}/users'>users</a>: {{nb_users}}</li>
    <li>number of <a href='/projects/{{ project['slug'] }}/records'>records</a>: {{nb_records}}</li>
</ul>
