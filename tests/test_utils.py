# coding: utf-8
"""This module provides test suite for :py:mod:`districounts.utils`."""


from districounts import utils


def test_api_version():
    """Test that decoration with api_version works."""

    # sample function
    def func(kwargs: dict = None):
        """Return the input dict."""
        kwargs = kwargs or {}
        return kwargs

    dec_f = utils.api_versions('v1')(func)
    assert dec_f({}) == {'api_versions': ['v1']}
    assert dec_f.__doc__ == func.__doc__
    assert dec_f({'abo': 134}) == {'abo': 134, 'api_versions': ['v1']}

    dec_f1 = utils.api_versions('v1', 'v2')(func)
    assert dec_f1({}) == {'api_versions': ['v1', 'v2']}
