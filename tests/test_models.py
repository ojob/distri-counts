# coding: utf-8
"""Test module for :py:mod:`districounts.models`."""

import json
import uuid
from decimal import Decimal
from pathlib import Path

import pytest

from districounts import models


HERE = Path(__file__).parent
TEST_IN = HERE / 'inputs'
TEST_OUT = HERE / 'outputs'


def test_user_id():
    """Test UserId class."""
    # all users shall have unique uid
    many_uids = [models.UserId() for _ in range(10**5)]
    assert len(set(many_uids)) == len(many_uids)


def test_user():
    """Test the models for user."""
    with pytest.raises(models.UserCreateError):
        models.User(name='')


def test_user_store():
    """Test the models for users."""

    # empty UserStore shall indicate it nicely
    us = models.UserStore()
    assert str(us) == 'UserStore(no user)'
    assert not us, "UserStore with no user shall be considered as empty"

    # UserStore shall provide functon to store existing User
    user_ana = models.User(name='ana')
    us.add(user=user_ana)
    assert str(us) == 'UserStore(1 user)'

    # UserStore shall not accept storage of two User instances with same uid
    user_ken = models.User(name='ken')
    user_ken.uid = user_ana.uid
    with pytest.raises(ValueError):  # cannot add two users with same id
        us.add(user=user_ken)

    # UserStore shall allow creation of User instances from parameters
    us.create(name='joe', email='a@doc.cm')
    us.create(name='bob', email='b@doc.cm')
    # UserStore shall allow creation of User instances with same parameters
    us.create(name='joe')

    # UserStore shall indicate how many users are stored
    assert len(us) == 4
    assert str(us) == 'UserStore(4 users)'

    # UserStore shall allow query by name, returning list of User instances
    resp = us['joe']
    assert isinstance(resp, list)
    # and check this is the expected user
    assert set(user.email for user in us['joe']) == {'a@doc.cm', None}
    # check that non-existing user query fails
    with pytest.raises(KeyError):
        _ = us['non-existing-user-name']

    # UserStore shall allow query by uid, returning one User instance
    # check that uid is the expected one
    user_bob = us['bob'][0]
    assert isinstance(user_bob, models.User)
    assert us[user_bob.uid].email == 'b@doc.cm'
    # check that non-existing user query fails
    with pytest.raises(KeyError):
        _ = us[10407210841]

    # UserStore shall allow iteration on User instances
    users_names = [user.uid for user in us]
    assert len(users_names) == len(us)

    # UserStore shall allow update of User charactesticts
    last_user_bob = us['bob'][-1]
    resp = us.update(uid=last_user_bob.uid, name='bob-roger')
    assert resp.name == 'bob-roger'
    assert resp.email == last_user_bob.email
    resp = us.update(uid=last_user_bob.uid, email='bob@roger.net')
    assert resp.email == 'bob@roger.net'
    # no issue raised if nothing to modify
    unmodified_user = us.update(uid=last_user_bob.uid)
    assert resp == unmodified_user

    # UserStore shall allow deletion of User instances
    us.create(name='roger')
    us.create(name='roger')
    roger_users = us['roger']
    assert len(roger_users) == 2
    resp = us.delete(uid=roger_users[0].uid)
    assert resp == roger_users[0]
    assert len(us['roger']) == 1
    with pytest.raises(KeyError):
        _ = us[roger_users[0].uid]
    with pytest.raises(models.UserDeleteError):
        us.delete(uid=roger_users[0].uid)


def test_user_store_tests_sample():
    """Test the creation of UserStore test sample."""
    us = models.UserStore()
    us.create_sample(nb_entries=5)
    assert len(us) == 5


def test_counts_tracker_test_sample():
    """Test the creation of RecordStore test sample."""
    us = models.UserStore()
    rs = models.RecordStore(users=us)
    us.create_sample(nb_entries=7)
    rs.create_sample(nb_entries=35)
    assert len(rs) == 35

    sp = models.SharesProject(name='other project')
    sp.create_sample(nb_entries=20)
    assert len(sp.users) == len(sp.records) == 20


def test_shares_project():
    """Test the accounting performed by SharesProject."""
    sp = models.SharesProject(name='project')
    sp.users.create_sample(nb_entries=4)
    uids = list(sp.users.keys())

    # RecordStore with no transaction shall be empty
    assert not sp.records

    # test command: create, with amount in float
    rid_0 = sp.create_record(
        sender=uids[3], receivers=[uids[2]], amount=2.0)
    # test direct public side effects
    assert len(sp.records) == 1
    assert sp.total() == 2.0
    assert sp.total(sender=uids[3]) == 2.0
    assert sp.total(sender=uids[2]) == 0.0
    assert isinstance(sp.records[rid_0].amount, Decimal)  # check amount type

    # check exception at accessing unknown records
    with pytest.raises(models.RecordAccessError):
        _ = sp.records['whatever']
    with pytest.raises(models.RecordAccessError):
        some_uuid = uuid.uuid4()
        assert some_uuid not in sp.records.keys()  # in case we're out of luck!
        _ = sp.records[some_uuid]

    # test returned exception at Record creation failure
    with pytest.raises(models.RecordCreateError):
        # sender shall be UserId-able
        _ = sp.create_record(
            sender='one', receivers=[uids[3]], amount=10.0)
    with pytest.raises(models.RecordCreateError):
        # receivers shall be iterable
        _ = sp.create_record(
            sender=uids[3], receivers=uids[3], amount=10.3)
    with pytest.raises(models.RecordCreateError):
        # amount shall be usable for creation of a Decimal instance
        _ = sp.create_record(
            sender=uids[3], receivers=[uids[3]], amount='10..3')
    with pytest.raises(models.RecordCreateError):
        # there shall be at least one receiver
        _ = sp.create_record(sender=uids[0], receivers=[], amount=10.3)

    # amount shall be exported as float, for JSON usage
    assert isinstance(sp.records[rid_0].as_dict()['amount'], float)

    # test query: total
    assert sp.total(sender=uids[2]) == 0.0
    assert sp.total(sender=uids[3]) == 2.0
    # test query: users_balance
    assert sp.users_balance() == \
        {uids[0]: 0.0, uids[1]: 0.0, uids[2]: -2.0, uids[3]: 2.0}

    # test transaction update inconsistency
    with pytest.raises(models.RecordUpdateError):
        # unknown user
        unk_uid = 10**6
        assert unk_uid not in sp.users
        _ = sp.update_record(rid=rid_0, sender=unk_uid)

    # test transaction update, with amount as int
    sp.update_record(rid=rid_0, sender=uids[1], amount=1)
    # test transaction update, with amount as float
    sp.update_record(rid=rid_0, sender=uids[1], amount=1.0)
    # test transaction update, with amount as Decimal
    sp.update_record(rid=rid_0, sender=uids[1], amount=Decimal('1.0'))
    # test query: total
    assert sp.total(sender=uids[1]) == 1.0
    assert sp.total(sender=uids[2]) == 0.0
    assert sp.total(sender=uids[3]) == 0.0
    # test query: users_balance
    assert sp.users_balance() == \
        {uids[0]: 0.0, uids[1]: 1.0, uids[2]: -1.0, uids[3]: 0.0}
    # check sanity
    assert sum(sp.users_balance().values()) == 0.0
    # test query: (project) balance
    assert sp.balance() == [(uids[2], uids[1], 1.0)]

    # test balance with two transactions
    rid_1 = sp.create_record(
        sender=uids[1], receivers=[uids[1], uids[2], uids[3]], amount=6)
    assert sp.users_balance() == \
        {uids[0]: 0.0, uids[1]: 5.0, uids[2]: -3.0, uids[3]: -2.0}
    # test query: (project) balance
    assert sp.balance() == [
        (uids[2], uids[1], 3.0),
        (uids[3], uids[1], 2.0),
        ]

    # modify receivers with a new user
    sp.create_user(name='bobi-joe')
    uids = list(sp.users.keys())
    sp.update_record(rid=rid_1, receivers=[uids[1], uids[3], uids[4]])
    exp_balance_0 = {uids[0]: 0.0, uids[1]: 5.0, uids[2]: -1.0, uids[3]: -2.0,
                     uids[4]: -2.0}
    assert sp.users_balance() == exp_balance_0

    # test query: (project) balance, in complex case
    rid_2 = sp.create_record(
        sender=uids[3], receivers=[uids[2], uids[3]], amount=Decimal('5.0'))
    assert sp.users_balance() == \
        {uids[0]: 0.0, uids[1]: 5.0, uids[2]: -3.5, uids[3]: 0.5,
         uids[4]: -2.0}
    assert sp.balance() == [
        (uids[2], uids[1], 3.5),
        (uids[4], uids[1], 1.5),
        (uids[4], uids[3], 0.5),
        ]

    # test command: delete
    rec = sp.delete_record(rid=rid_2)
    assert rec.sender == uids[3]
    assert sp.users_balance() == exp_balance_0

    # test refusal to specify an unknown user as part of a record
    unk_user = models.User(name='unknown!')
    with pytest.raises(models.RecordCreateError):
        _ = sp.create_record(
            sender=unk_user.uid, receivers=[uids[0], uids[1]], amount=10.5)

    # test balance when a user has no part in a record
    new_uid = sp.create_user(name='new user')
    uids = list(sp.users.keys())
    rid_3 = sp.create_record(
        sender=uids[0], receivers=[new_uid], amount=10.0)
    # first check that it is correctly listed
    assert new_uid in sp.users
    assert sp.users_balance()[new_uid] == -10.0
    balance = sp.balance()
    assert any([new_uid in bal for bal in balance])
    # then remove the transaction concerning it
    sp.delete_record(rid=rid_3)
    assert new_uid in sp.users
    assert sp.users_balance()[new_uid] == 0.0
    balance = sp.balance()
    assert not any([new_uid in bal for bal in balance])


def test_userstore_file_io():
    """Test initialization and dump of `UserStore` to/from file."""
    json_in_path = TEST_IN / 'districounts-project-load.json'
    json_out_path = TEST_OUT / 'userstore-project-dump.json'

    # load from JSON and check content
    with json_in_path.open('r') as json_fh:
        json_data = json.load(json_fh)
    us = models.UserStore(json_data=json_data)
    assert len(us) == 6

    uid_0 = models.UserId(12)
    assert uid_0 in us
    assert us[uid_0].name == 'Bob Samantha'
    assert us[uid_0].email is None

    uid_1 = models.UserId(1031832)
    assert uid_1 in us
    assert us[uid_1].name == 'Élisabette'
    assert us[uid_1].email == 'some@address.mail'

    # dump to JSON file
    if json_out_path.exists():
        json_out_path.unlink()  # missing_ok arg added in Python 3.8
    json_out_path.parent.mkdir(exist_ok=True)
    with json_out_path.open('w') as json_fh:
        json.dump(us.dump(), json_fh)
    assert json_out_path.is_file()
    # and check that reloading it creates same objects
    with json_out_path.open('r') as json_fh:
        json_data = json.load(json_fh)
    us_2 = models.UserStore()
    us_2.load(json_data=json_data)
    assert us_2 == us


def test_RecordStore_file_io():
    """Test initialization and dump of `RecordStore` to/from file."""
    json_in_path = TEST_IN / 'districounts-project-load.json'
    json_out_path = TEST_OUT / 'RecordStore-project-dump.json'

    # load from JSON and check content
    with json_in_path.open('r') as json_fh:
        json_data = json.load(json_fh)
    sp = models.SharesProject(name='new project', json_data=json_data)
    assert len(sp.users) == 6
    assert len(sp.records) == 2

    rid_0 = models.RecordId('aad1b2b4-b78f-4c50-8bc1-6a7574d7e5c3')
    assert rid_0 in sp.records
    assert sp.records[rid_0].sender == 12
    assert sp.records[rid_0].receivers == [5, 6, 12]
    assert sp.records[rid_0].amount == Decimal('103.13')

    rid_1 = models.RecordId('6dd0323c-0b99-44c7-84fd-fa69e333957a')
    assert rid_1 in sp.records
    assert sp.records[rid_1].sender == 1031832
    assert sp.records[rid_1].receivers == [7, 8, 1031832]
    assert sp.records[rid_1].amount == Decimal('40.00')

    # check correct computation of balances, with the slight unbalances
    # between "receivers" due to rounding at two figures
    exp_balance = {
        models.UserId(uid=5): Decimal('-34.38'),
        models.UserId(uid=6): Decimal('-34.38'),
        models.UserId(uid=7): Decimal('-13.33'),
        models.UserId(uid=12): Decimal('+68.76'),
        models.UserId(uid=8): Decimal('-13.33'),
        models.UserId(uid=1031832): Decimal('+26.66'),
       }
    assert sp.users_balance() == exp_balance

    # dump to JSON file
    if json_out_path.exists():
        json_out_path.unlink()  # missing_ok arg added in Python 3.8
    json_out_path.parent.mkdir(exist_ok=True)
    with json_out_path.open('w') as json_fh:
        json.dump(sp.dump(), json_fh)
    assert json_out_path.is_file()
    # and check that reloading it creates same objects
    with json_out_path.open('r') as json_fh:
        json_data = json.load(json_fh)
    sp_2 = models.SharesProject(name='some project', json_data=json_data)
    assert sp_2 == sp


def test_projects():
    """Test models.Projects class."""
    projects = models.Projects()
    project = projects.create_project(name='some **NEW!! project  ')
    initial_slug = project.slug
    assert initial_slug == 'some-new-project'

    # update the name of the project
    new_slug = projects.update_project(
        slug=project.slug, new_name='==SOME==new==PROJECT__WOOOOH')
    assert new_slug == 'some-new-project-wooooh'
    # get project by its new slug
    proj = projects.get_project(slug=new_slug)
    assert proj is project

    # request with an unknown slug returns appriopriate error
    with pytest.raises(models.ProjectAccessError):
        _ = projects.get_project(slug='an unexisting slug')

    # get project by old slug
    proj = projects.get_project(slug=initial_slug)
    assert proj is project
    # get project by old old slug, using its long history
    newnew_slug = projects.update_project(
        slug=new_slug, new_name='a normal project')
    assert newnew_slug == 'a-normal-project'
    proj = projects.get_project(slug=new_slug)
    assert proj is project
    proj = projects.get_project(slug=initial_slug)
    assert proj is project

    # delete a project
    proj = projects.delete_project(slug=newnew_slug)
    # verify that all history is cleaned
    with pytest.raises(models.ProjectAccessError):
        _ = projects.get_project(slug=initial_slug)
        _ = projects.get_project(slug=new_slug)
        _ = projects.get_project(slug=newnew_slug)
