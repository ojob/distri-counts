"""Script for deployment of a webapp on Pythonanywhere.

Set the appropriate environment variables if you expect this script to work!

This script exits with a return value from :py:func:`deploy`.

"""
# ---- built-in imports ---------------------------------------------------

import logging
import os
import sys
import time

# ---- third-party imports ------------------------------------------------
import requests


# ---- functions definition -----------------------------------------------
def deploy():
    """Deploy to pythonanywhere.

    Returns:
        0: if everything went smoothly
        1: if the commands to refresh the git repo on pythonanywhere failed
        2: if the command to reload the webapp on pythonanywhere failed
        3: if the webapp did not answer with status "200 OK"
        4: if the webapp did not indicate itself as "healthy"
        5: if the webapp version is not the one of the Git Tag

    """
    logger = logging.getLogger("deploy-script")

    logger.info("loading environment variables...")
    token = os.environ['DEPLOY_TOKEN']
    api_url = os.environ['DEPLOY_URL_API']
    prod_url = os.environ['DEPLOY_URL_PROD']
    username = os.environ['DEPLOY_USER']
    cons_id = os.environ['DEPLOY_CONSOLE_ID']
    domain_name = os.environ['DEPLOY_DOMAIN']

    tag = os.environ.get('CI_COMMIT_TAG', "")  # not always set
    commit = os.environ['CI_COMMIT_SHA']
    if tag:
        git_ref_name, git_ref, git_ref_short = 'tag', tag, tag
    else:
        git_ref_name, git_ref, git_ref_short = 'commit', commit, commit[:8]

    logger.info("refreshing the git repo and activating %s:%s...",
                git_ref_name, git_ref_short)
    resp = requests.post(
        url=f"{api_url}/api/v0/user/{username}/consoles/{cons_id}/send_input/",
        data={"input": '\n'.join([
            "cd ~/districounts",
            "git fetch",
            f"git checkout {git_ref}",
            ""  # empty line, to validate previous command
            ])},
        headers={'Authorization': f'Token {token}'},
        )
    if resp.status_code != 200:
        logging.error("failed to refresh git repo: %s", resp.text)
        return 1

    logger.info("triggering the webapp reload...")
    resp = requests.post(
        url=f"{api_url}/api/v0/user/{username}/webapps/{domain_name}/reload/",
        headers={'Authorization': f'Token {token}'},
        )
    if resp.status_code != 200:
        logging.error("failed to trigger reload: %s", resp.text)
        return 2

    logger.info("waiting a little, while the webapp is reloaded...")
    time.sleep(15)

    logger.info("checking that deploy went correctly...")
    resp = requests.get(url=f"{prod_url}/api/v1/healthy")
    if not resp.status_code == 200:
        logger.error("app healthy answer is not OK: %s", resp.text)
        return 3
    if not resp.json()['result']['healthy']:
        logger.error("app is not healthy: %s", resp.json())
        return 4
    app_version = resp.json()['app_version']
    if tag:
        if app_version != tag:
            logger.error("app version: %s, but expected %s (full answer: %s)",
                         app_version, tag, resp.json())
            return 5
        else:
            logger.info("app version: %s, as expected", app_version)
    else:
        logger.info("app version: %s", app_version)

    logger.info("deploy OK")
    return 0


# ---- auto-start ---------------------------------------------------------
if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    sys.exit(deploy())
