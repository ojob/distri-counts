README
======

``districounts`` is a hobby project, to learn how to develop multi-platform
app for counts between friends, just like Tricount_ or other similar on-line
and mobile applications.

.. Note::
    THIS IS CURRENTLY IN PREPARATION, AND DOES ONLY PART OF WHAT IS
    ADVERTISED BELOW.

    Please refer to the issues_ page for more information about what's left
    to develop.

This is not aimed at bringing something anew to the world, therefore:

- As I'm a pythonista_, so I'll go on with this language I much appreciate
  for back-end;
- I'll use Javascript for front-end.

The code of this project is hosted on `my framagit page`_, and the
documentation is hosted on related `Gitlab Page`_. As this is a toy project,
the packaged code is deployed on the test server of the `Python Package Index`_.

.. _Tricount: https://tricount.com
.. _issues: https://framagit.org/ojob/districounts/-/issues
.. _pythonista: https://python.org
.. _my framagit page: https://framagit.org/ojob/districounts/
.. _Gitlab Page: https://ojob.frama.io/districounts/
.. _Python Package Index: https://test.pypi.org/project/districounts/


Usage
-----

Once you performed the installation_ on a given server:

1.  Activate the virtual environment (as explained in installation)
2.  Start the server (blocking command)::

     $ districounts serve

3.  Access (http://localhost:8080/) (if it's not opened automatically)
4.  Create a new project:

    - give it a name
    - give it a description
    - add people's names to the tracker (members).

5.  Pick your name
6.  Add a new expense:

    - give it a description
    - give it a date
    - give it a value
    - specify the distribution across members.

7.  Check the tracker's balance
8.  Share the link to other people
9.  Enjoy!


Installation
------------

This application is based on Python, so in order to set it up properly, it
is strongly advised to set-up a virtual environment. This will permit
installation of dependencies without any possibility to break your system's
python installation.

1.  Ensure ``python`` (or ``python3``) is a command available on the computer
    that will act as your server.
2.  Create a folder of your choice and ``cd`` in it
3.  Create a new virtual environment:

    - if ``python3`` is your default version::

        $ venv .env

    - otherwise::

        $ python3 -m venv .env

4.  Activate your new virtual environment:

    - for Windows::

        $ .env\Scripts\activate.bat

    - for Linux::

        $ source .env/bin/activate

5.  Install the package:

    - currently hosted on test package index::

        pip install -i https://test.pypi.org/simple/ districounts

    - later (hopefully!) from the standard package index::

        $ pip install districounts

.. _Bottle: https://pypi.org/project/bottle/
