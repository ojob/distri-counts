CONTRIBUTING
============

To propose an update, you shall first test the stuff you propose, and for
this local testing is available:

1.  Follow the installation procedure, in particular by creating a dedicated
    virtual environment. Activate it.
2.  Install the test dependencies::

        $ pip install -r dev-requirements.txt

3.  Check if the tests pass::

        $ tox

4.  If everything works, create a git branch, and start doing stuff.
5.  When you're happy with your modifications, play ``$ tox`` again; regression
    in testing coverage and code quality shall be avoided.
6.  Create a Merge Request in Gitlab.

If your modifications are accepted, then the maintainer will:

7.  Update the ``VERSION`` file content and accept the Merge Request

To release a version of the package:

8.  Tag the commit with a tag. They're all protected, and will trigger
    additional CD steps:

    - `deploy-test` stage:

      * deployment to the test Pypi server, including test of retrieval;

    - `deploy-prod` stage:

      * generation of documentation pages and deployment to Gitlab Pages,
      * deployment to the production Pypi server,
      * deployment to the production server.

If you wish to build and deploy from local computer, you may do as follows:

1.  Create the build::

        $ python3 setup.py sdist bdist_wheel

2.  Publish the new version to PyPi, first starting by the test server::

        $ twine upload --repository-url https://test.pypi.org/legacy/ dist/*
        $ twine upload dist/*

    .. Note::
        this call uses environment variables ``TWINE_USERNAME`` and
        ``TWINE_PASSWORD``.

        In Gitlab, the same commands are used, as these variables are declared
        as protected, they are only reachable by for jobs related to proctected
        tags or branches, i.e. the command will fail if launched
        from a 'normal' commit.

        See `this gitlab forum post`_ for reference.

.. _this gitlab forum post: https://forum.gitlab.com/t/cannot-make-it-work-uploading-to-pypi-from-gitlab-pipelines/14179/5
